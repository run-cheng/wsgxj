package com.wsgxj.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.wsgxj.domain.ZjTownBackStreets;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ZjTownBackStreetsMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(ZjTownBackStreets record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(ZjTownBackStreets record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    ZjTownBackStreets selectByPrimaryKey(String id);

    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(ZjTownBackStreets record);

    /**
     * 根据名称查询
     * @param name
     * @return
     */
    ZjTownBackStreets findByName(@Param("name")String name);

    ZjTownBackStreets findById(@Param("id") String id);

    List<ZjTownBackStreets> findAll();

}