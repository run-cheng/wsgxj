package com.wsgxj.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Date;

import com.wsgxj.domain.QyProfit;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QyProfitMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(QyProfit record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(QyProfit record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    QyProfit selectByPrimaryKey(String id);

    /**
     * 根据主键动态更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(QyProfit record);

    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(QyProfit record);

    /**
     * 根据外键和时间查询
     * @param enterpriseId
     * @param time
     * @return
     */
    QyProfit findByEnterpriseIdAndTime(@Param("enterpriseId")String enterpriseId,@Param("time")Date time);

    /**
     * 根据企业进行查询
     * @param enterpriseId
     * @return
     */
    List<QyProfit> findByEnterpriseId(@Param("enterpriseId")String enterpriseId);


    List<QyProfit> selectByEnterpriseId(@Param("offset") Integer offset,@Param("limit") Integer limit,@Param("id") String id);

    Long count(String id);
}