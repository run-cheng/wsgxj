package com.wsgxj.mapper.sys;

import com.wsgxj.domain.sys.SysRole;

import java.util.List;

public interface SysRoleMapper {
    int deleteByPrimaryKey(Long roleId);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long roleId);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    List<SysRole> find(Integer offset, Integer limit, String roleName);

    Long count(String roleName);

    void deleteBatch(Long[] ids);

    List<SysRole> findAll();

    List<String> selectRoleNameList(Long userId);

    void addSysRole(SysRole sysRole);
}