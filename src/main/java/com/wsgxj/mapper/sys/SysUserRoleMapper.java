package com.wsgxj.mapper.sys;

import com.wsgxj.domain.sys.SysUserRole;

import java.util.List;

public interface SysUserRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);

    void deleteByRoleIds(Long[] ids);

    void deleteByUserIds(Long[] userIds);

    List<Long> findRoleList(Long userId);

    List<String> findRoleListByUserId(Long userId);
}