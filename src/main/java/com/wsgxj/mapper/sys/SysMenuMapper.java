package com.wsgxj.mapper.sys;

import com.wsgxj.domain.sys.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMenuMapper {
    int deleteByPrimaryKey(Long menuId);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(Long menuId);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKey(SysMenu record);

    List<String> getUserPermsList(Long userId);

    List<SysMenu> find(@Param("offset") Integer offset,@Param("limit") Integer limit,@Param("menuName") String menuName);

    Long count(String menuName);

    void deleteBatch(Long[] ids);

    List<SysMenu> getNotButtonList();

    List<SysMenu> getTopMenuList(Long id);

    List<SysMenu> findAll();




}