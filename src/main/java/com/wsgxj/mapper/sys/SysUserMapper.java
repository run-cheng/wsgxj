package com.wsgxj.mapper.sys;

import com.wsgxj.domain.sys.SysUser;

import java.util.List;

public interface SysUserMapper {
    int deleteByPrimaryKey(Long userId);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser getByUsername(String username);

    List<SysUser> find(Integer offset, Integer limit, Long createUserId, String userName);

    Long count(Long createUserId, String userName);

    void deleteBatch(Long[] userIds);

    String getPasswordById(Long userId);

    void addSysUser(SysUser sysUser);
}