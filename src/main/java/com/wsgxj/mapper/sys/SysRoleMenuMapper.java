package com.wsgxj.mapper.sys;

import com.wsgxj.domain.sys.SysRoleMenu;

import java.util.List;

public interface SysRoleMenuMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysRoleMenu record);

    int insertSelective(SysRoleMenu record);

    SysRoleMenu selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRoleMenu record);

    int updateByPrimaryKey(SysRoleMenu record);

    void deletByMenuIds(Long[] ids);

    void deleteByRoleIds(Long[] ids);

    List<Long> findMenuIdList(Long roleId);

    List<Long> findMenuIdListByRoleId(Long roleId);
}