package com.wsgxj.mapper.sys;

import com.wsgxj.domain.sys.SysConfig;

import java.util.List;

public interface SysConfigMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysConfig record);

    int insertSelective(SysConfig record);

    SysConfig selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysConfig record);

    int updateByPrimaryKey(SysConfig record);

    List<SysConfig> findByKeyPrefix(String keyPrefix);

    List<SysConfig> find(Integer offset, Integer limit);

    Long count();

    void deleteBatch(Long[] ids);
}