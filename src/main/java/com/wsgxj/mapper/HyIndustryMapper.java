package com.wsgxj.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.wsgxj.domain.HyIndustry;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HyIndustryMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(HyIndustry record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(HyIndustry record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    HyIndustry selectByPrimaryKey(String id);

    /**
     * 根据主键动态更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(HyIndustry record);

    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(HyIndustry record);

    /**
     * 根据名称查询
     * @param name
     * @return
     */
    HyIndustry findByName(@Param("name")String name);

    /**
     * 根据企业id查询
     * @param industryId
     * @return
     */
    List<HyIndustry> findByIndustryId(@Param("industryId")String industryId);

    List<HyIndustry> find(@Param("offset") Integer offset,@Param("limit") Integer limit, @Param("name") String name);

    Long count(@Param("name") String name);

    HyIndustry findById(String id);
}