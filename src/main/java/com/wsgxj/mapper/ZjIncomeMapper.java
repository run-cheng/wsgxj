package com.wsgxj.mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Date;
import java.util.Map;

import com.wsgxj.domain.ZjIncome;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ZjIncomeMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(ZjIncome record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(ZjIncome record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    ZjIncome selectByPrimaryKey(String id);

    /**
     * 根据主键动态更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(ZjIncome record);

    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(ZjIncome record);

    /**
     * 根据外键和时间查询
     * @param townBackStreetsId
     * @param time
     * @return
     */
    ZjIncome findByTownBackStreetsIdAndTime(@Param("townBackStreetsId")String townBackStreetsId,@Param("time")Date time);

    /**
     * 根据镇街查询
     * @param townBackStreetsId
     * @return
     */
    List<ZjIncome> findByTownBackStreetsId(@Param("townBackStreetsId")String townBackStreetsId);


    List<Map<String, Object>> findByTime(HashMap<String, String> dateMap);
}