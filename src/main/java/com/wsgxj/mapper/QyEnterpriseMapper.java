package com.wsgxj.mapper;
import com.wsgxj.domain.QyEnterprise;
import com.wsgxj.vo.EchartsDataResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface QyEnterpriseMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(QyEnterprise record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(QyEnterprise record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    QyEnterprise selectByPrimaryKey(String id);


    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(QyEnterprise record);

    /**
     * 根据主键动态更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(QyEnterprise record);

    /**
     * 根据企业名称查询
     * @param name
     * @return
     */
    QyEnterprise findByName(@Param("name")String name);

    /**
     * 根据镇街进行查询
     * @param zjTownBackStreetsId
     * @return
     */
    List<QyEnterprise> findByZjTownBackStreetsId(@Param("zjTownBackStreetsId")String zjTownBackStreetsId);

    /**
     * 根据行业代码分类统计个数
     * @param industryCodes
     * @return
     */
    int findByIndustryCode(@Param("industryCodes")String[] industryCodes);

    /**
     * 根据镇街，创建时间动态统计个数
     * @param creationTime
     * @return
     */
    List<Map<String,Object>> findByZjTownBackStreetsIdAndCreationTime(@Param("creationTime")Date creationTime);

    /**
     * 根据月份查询营收总额
     * @param presentMap 封装了月份第一天（first)和最后一天(last)
     * @return
     */
    Map<String,Double> findIncomeByMoth(Map<String, String> presentMap);

    /**
     * 根据月份查询利润总额
     * @param presentMap 封装了月份第一天（first)和最后一天(last)
     * @return
     */
    Map<String, Double> findProfitByMoth(Map<String, String> presentMap);

    /**
     * 根据时间查询各镇街营收占全县比重
     * @param timeMap 封装了开始时间(startTime)和结束时间(endTime)
     * @return
     */
    List<EchartsDataResult> getRevenueProportionTheCountyByTime(Map<String, String> timeMap);


    List<QyEnterprise> find(@Param("offset") Integer offset,@Param("limit") Integer limit, @Param("val") String val,@Param("id") String id);


    Long count(@Param("val") String val,@Param("id") String id);

    QyEnterprise findById(String enterpriseId);

    int deleteById(String id);
}