package com.wsgxj.mapper;
import java.util.List;

import com.wsgxj.domain.HyElectricityConsumption;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

@Mapper
public interface HyElectricityConsumptionMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(HyElectricityConsumption record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(HyElectricityConsumption record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    HyElectricityConsumption selectByPrimaryKey(String id);

    /**
     * 根据主键动态更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(HyElectricityConsumption record);

    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(HyElectricityConsumption record);

    /**
     * 根据外键，时间
     * @param industryId
     * @param time
     * @param proportionTheCounty
     * @return
     */
    int updateByIndustryIdAndTime(@Param("industryId")String industryId, @Param("time") Date time, @Param("proportionTheCounty")String proportionTheCounty);

    /**
     * 根据外键，时间查询
     * @param time
     * @param industryId
     * @return
     */
    HyElectricityConsumption findByTimeAndIndustryId(@Param("time")Date time,@Param("industryId")String industryId);


    List<HyElectricityConsumption> findByINdustryId(@Param("offset") Integer offset,@Param("limit") Integer limit, @Param("id") String id);

    Long count(String id);
}