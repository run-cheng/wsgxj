package com.wsgxj.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Date;
import java.util.Map;

import com.wsgxj.domain.ZjProfit;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ZjProfitMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(ZjProfit record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(ZjProfit record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    ZjProfit selectByPrimaryKey(String id);

    /**
     * 根据主键动态更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(ZjProfit record);

    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(ZjProfit record);

    /**
     * 根据外键和时间查询
     * @param townBackStreetsId
     * @param time
     * @return
     */
    ZjProfit findByTownBackStreetsIdAndTime(@Param("townBackStreetsId")String townBackStreetsId,@Param("time")Date time);

    /**
     * 根据镇街查询
     * @param townBackStreetsId
     * @return
     */
    List<ZjProfit> findByTownBackStreetsId(@Param("townBackStreetsId")String townBackStreetsId);

    List<Map<String,Object>> findByTime(Map<String,String> time);




}