package com.wsgxj.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import java.util.Date;

import com.wsgxj.domain.QyIncome;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QyIncomeMapper {
    /**
     * 根据主键删除
     * @param id
     * @return
     */
    int deleteByPrimaryKey(String id);

    /**
     * 插入数据
     * @param record
     * @return
     */
    int insert(QyIncome record);

    /**
     * 动态插入数据
     * @param record
     * @return
     */
    int insertSelective(QyIncome record);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    QyIncome selectByPrimaryKey(String id);

    /**
     * 根据主键动态更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(QyIncome record);

    /**
     * 根据主键更新数据
     * @param record
     * @return
     */
    int updateByPrimaryKey(QyIncome record);

    /**
     * 根据外键和时间更新行业比重
     * @param time
     * @param enterpriseId
     * @param proportionIndustry
     * @return
     */
    int updateByTimeAndEnterpriseId(@Param("time") Date time,@Param("enterpriseId") String enterpriseId,
                                    @Param("proportionIndustry") Double proportionIndustry);

    /**
     * 根据外键和时间查询
     * @param enterpriseId
     * @param time
     * @return
     */
    QyIncome findByEnterpriseIdAndTime(@Param("enterpriseId")String enterpriseId,@Param("time")Date time);

    /**
     * 根据企业进行查询
     * @param enterpriseId
     * @return
     */
    List<QyIncome> findByEnterpriseId(@Param("enterpriseId")String enterpriseId);

    List<QyIncome> selectByEnterpriseId(@Param("offset") Integer offset,@Param("limit") Integer limit,@Param("id") String id);

    Long count(String id);
}