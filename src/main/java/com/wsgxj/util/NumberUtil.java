package com.wsgxj.util;

import java.math.BigDecimal;

public class NumberUtil {
    private NumberUtil(){}

    //把数字四舍五入保留4位小数
    public static double getNumberRetain4(double num){
        BigDecimal b = new BigDecimal(num);
        double resultNumber = b.setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
        return resultNumber;
    }

    public static void main(String[] args) {
        System.out.println(getNumberRetain4(0.13454));
    }
}
