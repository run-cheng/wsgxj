package com.wsgxj.util;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.crypto.hash.SimpleHash;

public class PasswordUtil {

	public static String md5(String source, Object salt, int hashIterations) {
		//SimpleHash simpleHash = new SimpleHash("md5", source, salt, hashIterations);
		SimpleHash simpleHash = new Md5Hash(source, salt, hashIterations);
		String md5 = simpleHash.toString();
		return md5;
	}
	
	public static String md5(String source, Object salt) {
		SimpleHash simpleHash = new Md5Hash(source, salt, 1024);
		String md5 = simpleHash.toString();
		return md5;
	}
	
	public static void main(String[] args) {
		String md5 = md5("123456","admin");
		System.out.println(md5);
	}
}
