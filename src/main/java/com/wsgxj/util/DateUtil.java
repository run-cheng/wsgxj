package com.wsgxj.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 时间工具类
 */
public class DateUtil {

	public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String DATE_PATTERN_DATE = "yyyy.MM.dd";
	public static final String SIMPLE_DATE = "yyyy-MM-dd";

	public static String parseDateToString(Date date, String pattern){
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		String dateStr = format.format(date);
		return dateStr;
	}

	public static String parseStringToString(String date, String pattern)
			throws ParseException {
		Date parse = new SimpleDateFormat(DATE_PATTERN).parse(date);
		String dateStr = parseDateToString(parse, pattern);
		return dateStr;

	}

	// 获取当前日期的几天前或几天后日期
	public static String getCurrentBeforeNum(String pattern, int num) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, num);
		return parseDateToString(cal.getTime(), pattern);
	}

	//将字符串转换为时间对象
	public static Date parseStringToDate(String date,String pattern) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date parse = format.parse(date);
		return parse;
	}


	//获取一年的头一天
	public static Date getYearFirst(int year){
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, year);
		Date currYearFirst = calendar.getTime();
		return currYearFirst;

	}

	//获取当前时间的年份
	public static int getYear(){
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		return year;
	}

	//获取特定月的头一天和最后一天
	public static Map<String,String> getMonth(String time) throws ParseException {
		Date date = parseStringToDate(time, SIMPLE_DATE);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 0);
		Date theDate = calendar.getTime();

		//上个月第一天
		GregorianCalendar gcLast = (GregorianCalendar) Calendar.getInstance();
		gcLast.setTime(theDate);
		gcLast.set(Calendar.DAY_OF_MONTH, 1);
		String day_first = df.format(gcLast.getTime());
		StringBuffer str = new StringBuffer().append(day_first);
		day_first = str.toString();

		//上个月最后一天
		calendar.add(Calendar.MONTH, 1);    //加一个月
		calendar.set(Calendar.DATE, 1);        //设置为该月第一天
		calendar.add(Calendar.DATE, -1);    //再减一天即为上个月最后一天
		String day_last = df.format(calendar.getTime());
		StringBuffer endStr = new StringBuffer().append(day_last);
		day_last = endStr.toString();

		Map<String, String> map = new HashMap<String, String>();
		map.put("first", day_first);
		map.put("last", day_last);
		return map;
	}

	//根据yyyy-MM格式获取该月的头一天和最后一天
    public static Map<String,String> getFirstDayAndLastDayByYearAndMonth(String yearAndMonth){
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Map<String,String> resultMap = new HashMap<>();
        try {
            Date  currdate = sd.parse(yearAndMonth);

            //获取当前月第一天：
            Calendar calendar= Calendar.getInstance();
            calendar.setTime(currdate);
            calendar.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天
            String first = sdf.format(calendar.getTime());
            //System.out.println(first);
            resultMap.put("first", first);

            //获取当前月最后一天
            Calendar ca = Calendar.getInstance();
            ca.setTime(currdate);
            ca.set(Calendar.DAY_OF_MONTH, ca.getActualMaximum(Calendar.DAY_OF_MONTH));
            String last = sdf.format(ca.getTime());
            //System.out.println(last);
            resultMap.put("last", last);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultMap;
    }

	//获取最近12个月，经常用于统计图表的X轴
    public static List<String> getLast12Months(){
        List<String> last12Months = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1); //要先+1,才能把本月的算进去
        for(int i=0; i<12; i++){
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1); //逐次往前推1个月
            last12Months.add( cal.get(Calendar.YEAR)+ "-" + String.format("%02d",(cal.get(Calendar.MONTH)+1)));
        }
        return last12Months;
    }

    public static void main(String[] args) {

    }
}
