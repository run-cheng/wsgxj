package com.wsgxj.service;

import com.wsgxj.domain.QyProfit;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

import java.util.Date;

public interface QyProfitService{


    int deleteByPrimaryKey(String id);

    int insert(QyProfit record);

    int insertSelective(QyProfit record);

    QyProfit selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(QyProfit record);

    int updateByPrimaryKey(QyProfit record);

    QyProfit findByEnterpriseIdAndTime(String enterpriseId, Date time);

    DataGridResult getPageList(Query query);
}
