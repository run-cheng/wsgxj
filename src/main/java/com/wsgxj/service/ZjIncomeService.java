package com.wsgxj.service;

import com.wsgxj.domain.ZjIncome;

import java.util.Date;

public interface ZjIncomeService{


    int deleteByPrimaryKey(String id);

    int insert(ZjIncome record);

    int insertSelective(ZjIncome record);

    ZjIncome selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ZjIncome record);

    int updateByPrimaryKey(ZjIncome record);

    ZjIncome findByTownBackStreetsIdAndTime(String townBackStreetsId, Date time);

}
