package com.wsgxj.service;

import com.wsgxj.vo.EchartsDataResult;

import java.util.List;
import java.util.Map;

public interface EchartsDataService {

    List<EchartsDataResult> getThreeMajorCategories();

    List<EchartsDataResult> getKeyIndustries();

    Map<String,List> getEnterpriseNumber(Integer year);

    Map<String, List> getProfitGrowth(String start,String end);

    Map<String, List> getRevenueCumulativeGrowth();

    Map<String, List> getProfitGrowthHas();

    Map<String, List> getRevenueGrowth(String start,String end);

    List<EchartsDataResult> getRevenueProportionTheCounty(String start,String end);
}
