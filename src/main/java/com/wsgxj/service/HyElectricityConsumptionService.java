package com.wsgxj.service;

import com.wsgxj.domain.HyElectricityConsumption;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

public interface HyElectricityConsumptionService{


    int deleteByPrimaryKey(String id);

    int insert(HyElectricityConsumption record);

    int insertSelective(HyElectricityConsumption record);

    HyElectricityConsumption selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(HyElectricityConsumption record);

    int updateByPrimaryKey(HyElectricityConsumption record);

    DataGridResult getPageList(Query query);
}
