package com.wsgxj.service.impl;

import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.wsgxj.mapper.HyElectricityConsumptionMapper;
import com.wsgxj.domain.HyElectricityConsumption;
import com.wsgxj.service.HyElectricityConsumptionService;

import java.util.List;

@Service
public class HyElectricityConsumptionServiceImpl implements HyElectricityConsumptionService{

    @Resource
    private HyElectricityConsumptionMapper hyElectricityConsumptionMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return hyElectricityConsumptionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(HyElectricityConsumption record) {
        return hyElectricityConsumptionMapper.insert(record);
    }

    @Override
    public int insertSelective(HyElectricityConsumption record) {
        return hyElectricityConsumptionMapper.insertSelective(record);
    }

    @Override
    public HyElectricityConsumption selectByPrimaryKey(String id) {
        return hyElectricityConsumptionMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(HyElectricityConsumption record) {
        return hyElectricityConsumptionMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(HyElectricityConsumption record) {
        return hyElectricityConsumptionMapper.updateByPrimaryKey(record);
    }

    @Override
    public DataGridResult getPageList(Query query) {
        Integer offset = (Integer) query.get("offset");
        Integer limit = (Integer) query.get("limit");
        String id = (String) query.get("id");
        List<HyElectricityConsumption> rows =  hyElectricityConsumptionMapper.findByINdustryId(offset,limit,id);

        Long total = hyElectricityConsumptionMapper.count(id);
        return new DataGridResult(total,rows);
    }

}
