package com.wsgxj.service.impl;

import com.wsgxj.domain.QyIncome;
import com.wsgxj.domain.QyProfit;
import com.wsgxj.domain.ZjTownBackStreets;
import com.wsgxj.mapper.QyIncomeMapper;
import com.wsgxj.mapper.QyProfitMapper;
import com.wsgxj.mapper.ZjTownBackStreetsMapper;
import com.wsgxj.util.ShiroUtil;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wsgxj.domain.QyEnterprise;
import com.wsgxj.mapper.QyEnterpriseMapper;
import com.wsgxj.service.QyEnterpriseService;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class QyEnterpriseServiceImpl implements QyEnterpriseService{

    @Autowired
    private QyEnterpriseMapper qyEnterpriseMapper;
    @Autowired
    private ZjTownBackStreetsMapper zjTownBackStreetsMapper;
    @Autowired
    private QyIncomeMapper qyIncomeMapper;
    @Autowired
    private QyProfitMapper qyProfitMapper;
    @Override
    public int deleteByPrimaryKey(String id) {
        return qyEnterpriseMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(QyEnterprise record) {
        return qyEnterpriseMapper.insert(record);
    }

    @Override
    public int insertSelective(QyEnterprise record) {
        String id = UUID.randomUUID().toString();
        record.setId(id);
        record.setCreationUserId((Long) ShiroUtil.getSessionAttribute("id"));
        record.setCreationTime(new Date());
        if (record.getDeleteId() == 1){
            record.setDeleteTime(new Date());
        }
        return qyEnterpriseMapper.insertSelective(record);
    }

    @Override
    public QyEnterprise selectByPrimaryKey(String id) {
        return qyEnterpriseMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(QyEnterprise record) {
        return qyEnterpriseMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(QyEnterprise record) {
        return qyEnterpriseMapper.updateByPrimaryKey(record);
    }

    @Override
    public QyEnterprise findByName(String name) {
        return qyEnterpriseMapper.findByName(name);
    }

    @Override
    public DataGridResult getPageList(Query query) {
        Integer offset = (Integer) query.get("offset");
        Integer limit = (Integer) query.get("limit");
        String val = (String) query.get("val");
        ZjTownBackStreets townBackStreets = zjTownBackStreetsMapper.findByName(val);
        if (townBackStreets != null){
            String id = townBackStreets.getId();
            //调用Dao查询分页列表数据
            List<QyEnterprise> rows = qyEnterpriseMapper.find(offset, limit, val,id);
            for (QyEnterprise row : rows) {
                row.setZjName(townBackStreets.getName());
            }
            //调用Dao查询总记录数
            Long total = qyEnterpriseMapper.count(val,id);
            //创建DataGridResult
            DataGridResult dataGridResult = new DataGridResult(total, rows);
            return dataGridResult;
        }
        List<QyEnterprise> rows = qyEnterpriseMapper.find(offset, limit, val, null);
        for (QyEnterprise row : rows) {
            ZjTownBackStreets zjTownBackStreets = zjTownBackStreetsMapper.findById(row.getZjTownBackStreetsId());
            row.setZjName(zjTownBackStreets.getName());
        }
        Long total = qyEnterpriseMapper.count(val, null);
        //创建DataGridResult
        DataGridResult dataGridResult = new DataGridResult(total, rows);
        return dataGridResult;
    }

    @Override
    public QyEnterprise findById(String enterpriseId) {
        return qyEnterpriseMapper.findById(enterpriseId);
    }

    @Override
    public void update(QyEnterprise qyEnterprise) {
        qyEnterpriseMapper.updateByPrimaryKey(qyEnterprise);
    }

    //根据数据进行逻辑删除
    @Override
    public void deleteBatch(String[] ids) {
        if (ids != null && ids.length != 0){
            for (int i = 0; i < ids.length; i++) {
                qyEnterpriseMapper.deleteById(ids[i]);
            }

        }
    }

    @Override
    public DataGridResult findByIdAll(Query query) {
        Integer offset = (Integer) query.get("offset");
        Integer limit = (Integer) query.get("limit");
        String id = (String) query.get("id");
        QyEnterprise qyEnterprise = new QyEnterprise();
        //分页查询营收记录
        List<QyIncome> qyIncomes = qyIncomeMapper.selectByEnterpriseId(offset, limit, id);
        qyEnterprise.setQyIncomes(qyIncomes);
        //分页查询利润记录
        List<QyProfit> qyProfits = qyProfitMapper.selectByEnterpriseId(offset, limit, id);
        qyEnterprise.setQyProfits(qyProfits);
        Long total = qyIncomeMapper.count(id);
        //创建DataGridResult
        //DataGridResult dataGridResult = new DataGridResult(total, qyEnterprise);
        //return dataGridResult;
        return null;
    }


}
