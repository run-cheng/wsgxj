package com.wsgxj.service.impl;

import com.wsgxj.domain.QyEnterprise;
import com.wsgxj.domain.ZjTownBackStreets;
import com.wsgxj.domain.sys.SysUser;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.wsgxj.mapper.HyIndustryMapper;
import com.wsgxj.domain.HyIndustry;
import com.wsgxj.service.HyIndustryService;

import java.util.List;
import java.util.UUID;

@Service
public class HyIndustryServiceImpl implements HyIndustryService{

    @Resource
    private HyIndustryMapper hyIndustryMapper;


    @Override
    public DataGridResult getPageList(Query query) {
        Integer offset = (Integer) query.get("offset");
        Integer limit = (Integer) query.get("limit");
        String name = (String) query.get("name");
        List<HyIndustry> rows = hyIndustryMapper.find(offset, limit, name);
        //调用Dao查询总记录
        Long total = hyIndustryMapper.count(name);
        //创建DataGridResult
        DataGridResult dataGridResult = new DataGridResult(total, rows);
        return dataGridResult;
    }

    @Override
    public void save(HyIndustry industry) {
        String id = UUID.randomUUID().toString();
        industry.setId(id);
        hyIndustryMapper.insertSelective(industry);
    }

    @Override
    public HyIndustry getById(String id) {
        return hyIndustryMapper.findById(id);
    }

    @Override
    public void update(HyIndustry hyIndustry) {
        hyIndustryMapper.updateByPrimaryKey(hyIndustry);
    }
}
