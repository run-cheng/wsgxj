package com.wsgxj.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.wsgxj.domain.ZjTownBackStreets;
import com.wsgxj.mapper.ZjTownBackStreetsMapper;
import com.wsgxj.service.ZjTownBackStreetsService;

import java.util.List;

@Service
public class ZjTownBackStreetsServiceImpl implements ZjTownBackStreetsService{

    @Resource
    private ZjTownBackStreetsMapper zjTownBackStreetsMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return zjTownBackStreetsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ZjTownBackStreets record) {
        return zjTownBackStreetsMapper.insert(record);
    }

    @Override
    public int insertSelective(ZjTownBackStreets record) {
        return zjTownBackStreetsMapper.insertSelective(record);
    }

    @Override
    public ZjTownBackStreets selectByPrimaryKey(String id) {
        return zjTownBackStreetsMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(ZjTownBackStreets record) {
        return zjTownBackStreetsMapper.updateByPrimaryKey(record);
    }

    @Override
    public ZjTownBackStreets findByName(String name) {
        return zjTownBackStreetsMapper.findByName(name);
    }

    @Override
    public List<ZjTownBackStreets> findAll() {
        return zjTownBackStreetsMapper.findAll();
    }

}
