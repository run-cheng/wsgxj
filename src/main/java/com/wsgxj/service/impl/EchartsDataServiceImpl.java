package com.wsgxj.service.impl;

import com.wsgxj.mapper.QyEnterpriseMapper;
import com.wsgxj.mapper.ZjIncomeMapper;
import com.wsgxj.mapper.ZjProfitMapper;
import com.wsgxj.service.EchartsDataService;
import com.wsgxj.util.DateUtil;
import com.wsgxj.util.NumberUtil;
import com.wsgxj.vo.EchartsDataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class EchartsDataServiceImpl implements EchartsDataService {

    //企业
    @Autowired
    private QyEnterpriseMapper qyEnterpriseMapper;
    //镇街利润
    @Autowired
    private ZjProfitMapper zjProfitMapper;
    //镇街营收
    @Autowired
    private ZjIncomeMapper zjIncomeMapper;

    @Override
    public List<EchartsDataResult> getThreeMajorCategories() {
        ArrayList<EchartsDataResult> echartsDataResults = new ArrayList<>();
        int count;
        //采矿业  6-12
        String[] miningIndustrys = new String[]{"6","7","8","9","10","11","12"};
        count = qyEnterpriseMapper.findByIndustryCode(miningIndustrys);
        EchartsDataResult echartsData1 = new EchartsDataResult();
        echartsData1.setValue(count);
        echartsData1.setName("采矿业企业");
        echartsDataResults.add(echartsData1);
        //电热气水生产供应业
        String[] productions = new String[]{"44","45","46"};
        count = qyEnterpriseMapper.findByIndustryCode(productions);
        EchartsDataResult echartsData2 = new EchartsDataResult();
        echartsData2.setValue(count);
        echartsData2.setName("电热气水生产供应业企业");
        echartsDataResults.add(echartsData2);
        //制造业 13-43
        String[] manufacturs = new String[31];
        int index = 0;
        for (int i = 13; i <= 43; i++) {
            manufacturs[index] = i + "";
            index++;
        }
        count = qyEnterpriseMapper.findByIndustryCode(manufacturs);
        EchartsDataResult echartsData3 = new EchartsDataResult();
        echartsData3.setValue(count);
        echartsData3.setName("制造业企业");
        echartsDataResults.add(echartsData3);
        return echartsDataResults;
    }

    @Override
    public List<EchartsDataResult> getKeyIndustries() {
        ArrayList<EchartsDataResult> echartsDataResults = new ArrayList<>();
        int count;
        //非金属制品制造业  30
        String[] miningIndustrys = new String[]{"30"};
        count = qyEnterpriseMapper.findByIndustryCode(miningIndustrys);
        EchartsDataResult echartsData1 = new EchartsDataResult();
        echartsData1.setValue(count);
        echartsData1.setName("非金属制品制造企业");
        echartsDataResults.add(echartsData1);
        //纺织服装企业 18
        String[] productions = new String[]{"18"};
        count = qyEnterpriseMapper.findByIndustryCode(productions);
        EchartsDataResult echartsData2 = new EchartsDataResult();
        echartsData2.setValue(count);
        echartsData2.setName("纺织服装企业");
        echartsDataResults.add(echartsData2);
        //高端化工企业 26-27
        String[] manufacturs = new String[]{"26","27"};
        count = qyEnterpriseMapper.findByIndustryCode(manufacturs);
        EchartsDataResult echartsData3 = new EchartsDataResult();
        echartsData3.setValue(count);
        echartsData3.setName("高端化工企业");
        echartsDataResults.add(echartsData3);
        //装备制造企业 33-38
        String[] equipments = new String[]{"33","34","35","36","37","38"};
        count = qyEnterpriseMapper.findByIndustryCode(equipments);
        EchartsDataResult echartsData4 = new EchartsDataResult();
        echartsData4.setValue(count);
        echartsData4.setName("装备制造企业");
        echartsDataResults.add(echartsData4);
        return echartsDataResults;
    }

    @Override
    public Map<String,List> getEnterpriseNumber(Integer year) {
        HashMap<String, List> enterpriseNumber = new HashMap<>();
        //镇街名称集合
        List<String> nameList = new ArrayList<>();
        //全部企业
        ArrayList<Long> allNumList = new ArrayList<>();
        //今年新建企业
        ArrayList<Long> yearNumList = new ArrayList<>();
        //全部
        List<Map<String,Object>> allNum = qyEnterpriseMapper.findByZjTownBackStreetsIdAndCreationTime(null);
        //获取固定年份第一天
        Date date = null;
        if (year != null){
            date = DateUtil.getYearFirst(year);
        }
        //今年新建企业
        List<Map<String,Object>> yearNum = qyEnterpriseMapper.findByZjTownBackStreetsIdAndCreationTime(date);
        for (int i = 0; i < allNum.size(); i++) {
            Map<String, Object> map = allNum.get(i);
            String name = (String) map.get("name");
            Long num1 = (Long) map.get("num");
            nameList.add(name);
            allNumList.add(num1);
            Long num;
            for (int i1 = 0; i1 < yearNum.size(); i1++) {
                Map<String, Object> yearMap = yearNum.get(i1);
                String name1 = (String) yearMap.get("name");
                if (name1.equals(name)) {
                    num = (Long) yearMap.get("num");
                    yearNumList.add(num);
                    break;
                }
            }
            }
        enterpriseNumber.put("name",nameList);
        enterpriseNumber.put("allNum",allNumList);
        enterpriseNumber.put("yearNum",yearNumList);
        return enterpriseNumber;
    }

    //汶上县规上工业企业营收累计增幅
    @Override
    public Map<String, List> getRevenueCumulativeGrowth() {
        //定义返回map
        Map<String, List> resultMap = new HashMap<>();

        //处理x轴以当前月份逆推一年的月份
        List<String> xList = DateUtil.getLast12Months();
        resultMap.put("xList", xList);

        //根据x轴月份查询y值数据
        List<Double> yList = new ArrayList<>();
        for(int i=0; i<xList.size(); i++){
            //根据月份查询并计算企业营收累计增幅
            //营收累计增幅=（本月营收总额-去年同期营收总额）/ （去年同期营收总额） * 100%
            //1.获取本月营收总额和上月营收总额
            Map<String, String> presentMap = DateUtil.getFirstDayAndLastDayByYearAndMonth(xList.get(i));
            Map<String,Double> incomeMap = qyEnterpriseMapper.findIncomeByMoth(presentMap);
            //2.计算营收累计增幅
            Double presentIncome = incomeMap.get("presentIncome");
            Double lastYearIncome = incomeMap.get("lastYearIncome");
            double incomeGrow = 0;
            if(lastYearIncome != null && lastYearIncome != 0){
                incomeGrow = NumberUtil.getNumberRetain4((presentIncome - lastYearIncome) / lastYearIncome * 100);
            }
            yList.add(incomeGrow);
        }
        resultMap.put("yList", yList);
        return resultMap;
    }

    //汶上县规上工业企业利润累计增幅
    @Override
    public Map<String, List> getProfitGrowthHas() {
        //定义返回map
        Map<String, List> resultMap = new HashMap<>();

        //处理x轴以当前月份逆推一年的月份
        List<String> xList = DateUtil.getLast12Months();
        resultMap.put("xList", xList);

        //根据x轴月份查询y值数据
        List<Double> yList = new ArrayList<>();
        for(int i=0; i<xList.size(); i++){
            //根据月份查询并计算企业利润累计增幅
            //利润累计增幅=（本月利润总额-去年同期利润总额）/ （去年同期利润总额） * 100%
            //1.获取本月利润总额和上月利润总额
            Map<String, String> presentMap = DateUtil.getFirstDayAndLastDayByYearAndMonth(xList.get(i));
            Map<String,Double> profitMap = qyEnterpriseMapper.findProfitByMoth(presentMap);
            //2.计算营收累计增幅
            Double presentProfit = profitMap.get("presentProfit");
            Double lastYearProfit = profitMap.get("lastYearProfit");
            double profitGrow = 0;
            if(lastYearProfit != null && lastYearProfit != 0){
                profitGrow = NumberUtil.getNumberRetain4((presentProfit - lastYearProfit) / lastYearProfit * 100);
            }
            yList.add(profitGrow);
        }
        resultMap.put("yList", yList);
        return resultMap;
    }

    @Override
    public Map<String, List> getRevenueGrowth(String start,String end) {
        HashMap<String, String> dateMap = new HashMap<>();
        Map<String, List> dataMap = new HashMap<>();
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Double> growthList = new ArrayList<>();
        dateMap.put("first",start);
        dateMap.put("last",end);
        List<Map<String, Object>> dataList = zjIncomeMapper.findByTime(dateMap);
        //根据月份查询并计算企业营收累计增幅
        //营收累计增幅=（本月营收总额-去年同期营收总额）/ （去年同期营收总额） * 100%
        //1.获取本月营收总额和上月营收总额
        for (Map<String, Object> map : dataList) {
            String name = (String) map.get("name");
            nameList.add(name);
            Double thisnum = (Double) map.get("this");
            Double  last = (Double) map.get("last");
            double value = 0;
            if (last != null && last != 0) {
                //value = th.add(la).divide(la, 4, BigDecimal.ROUND_HALF_UP).divide(big, 4, BigDecimal.ROUND_HALF_UP).doubleValue();
                value = (thisnum -last) / last * 100;
                value = NumberUtil.getNumberRetain4(value);
            }
            growthList.add(value);
        }
        dataMap.put("name",nameList);
        dataMap.put("growth",growthList);
        return dataMap;
    }

    //各镇街营收占全县比重
    @Override
    public List<EchartsDataResult> getRevenueProportionTheCounty(String start,String end) {
        //定义返回结果集
        List<EchartsDataResult> resultList = new ArrayList<>();
        //按时间查询各镇街营收占全县比重
        Map<String, String> timeMap = new HashMap<>();
        timeMap.put("startTime", start);
        timeMap.put("endTime", end);
        resultList = qyEnterpriseMapper.getRevenueProportionTheCountyByTime(timeMap);
        return resultList;
    }

    @Override
    public Map<String, List> getProfitGrowth(String start,String end) {
        HashMap<String, String> dateMap = new HashMap<>();
        Map<String, List> dataMap = new HashMap<>();
        ArrayList<String> nameList = new ArrayList<>();
        ArrayList<Double> growthList = new ArrayList<>();
        dateMap.put("first",start);
        dateMap.put("last",end);
        List<Map<String, Object>> dataList = zjProfitMapper.findByTime(dateMap);
        //根据月份查询并计算企业营收累计增幅
        //营收累计增幅=（本月营收总额-去年同期营收总额）/ （去年同期营收总额） * 100%
        //1.获取本月营收总额和上月营收总额
        for (Map<String, Object> map : dataList) {
            String name = (String) map.get("name");
            nameList.add(name);
            Double thisnum = (Double) map.get("this");
            Double  last = (Double) map.get("last");
            double value = 0;
            if (last != null && last != 0) {
                //value = th.add(la).divide(la, 4, BigDecimal.ROUND_HALF_UP).divide(big, 4, BigDecimal.ROUND_HALF_UP).doubleValue();
                value = (thisnum -last) / last * 100;
                value = NumberUtil.getNumberRetain4(value);
            }
            growthList.add(value);
        }
        dataMap.put("name",nameList);
        dataMap.put("growth",growthList);
        return dataMap;
    }


}
