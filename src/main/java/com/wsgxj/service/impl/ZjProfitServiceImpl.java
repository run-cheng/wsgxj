package com.wsgxj.service.impl;

import com.wsgxj.domain.ZjProfit;
import com.wsgxj.mapper.ZjProfitMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.wsgxj.service.ZjProfitService;

import java.util.Date;

@Service
public class ZjProfitServiceImpl implements ZjProfitService{

    @Resource
    private ZjProfitMapper zjProfitMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return zjProfitMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ZjProfit record) {
        return zjProfitMapper.insert(record);
    }

    @Override
    public int insertSelective(ZjProfit record) {
        return zjProfitMapper.insertSelective(record);
    }

    @Override
    public ZjProfit selectByPrimaryKey(String id) {
        return zjProfitMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ZjProfit record) {
        return zjProfitMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ZjProfit record) {
        return zjProfitMapper.updateByPrimaryKey(record);
    }

    @Override
    public ZjProfit findByTownBackStreetsIdAndTime(String townBackStreetsId, Date time) {
        return zjProfitMapper.findByTownBackStreetsIdAndTime(townBackStreetsId, time);
    }

}
