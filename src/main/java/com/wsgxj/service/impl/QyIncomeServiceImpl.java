package com.wsgxj.service.impl;

import com.wsgxj.domain.QyIncome;
import com.wsgxj.domain.QyProfit;
import com.wsgxj.mapper.QyIncomeMapper;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.wsgxj.service.QyIncomeService;

import java.util.Date;
import java.util.List;

@Service
public class QyIncomeServiceImpl implements QyIncomeService{

    @Resource
    private QyIncomeMapper qyIncomeMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return qyIncomeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(QyIncome record) {
        return qyIncomeMapper.insert(record);
    }

    @Override
    public int insertSelective(QyIncome record) {
        return qyIncomeMapper.insertSelective(record);
    }

    @Override
    public QyIncome selectByPrimaryKey(String id) {
        return qyIncomeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(QyIncome record) {
        return qyIncomeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(QyIncome record) {
        return qyIncomeMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByTimeAndEnterpriseId(Date time, String enterpriseId, Double proportionIndustry) {
        return qyIncomeMapper.updateByTimeAndEnterpriseId(time,enterpriseId,proportionIndustry);
    }

    @Override
    public QyIncome findByEnterpriseIdAndTime(String enterpriseId, Date time) {
        return qyIncomeMapper.findByEnterpriseIdAndTime(enterpriseId,time);
    }

    @Override
    public DataGridResult getPageList(Query query) {
        Integer offset = (Integer) query.get("offset");
        Integer limit = (Integer) query.get("limit");
        String id = (String) query.get("id");
        List<QyIncome> rows =  qyIncomeMapper.selectByEnterpriseId(offset, limit, id);
        Long total = qyIncomeMapper.count(id);
        return new DataGridResult(total,rows);

    }

}
