package com.wsgxj.service.impl;

import com.wsgxj.domain.QyProfit;
import com.wsgxj.mapper.QyProfitMapper;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.wsgxj.service.QyProfitService;

import java.util.Date;
import java.util.List;

@Service
public class QyProfitServiceImpl implements QyProfitService{

    @Resource
    private QyProfitMapper qyProfitMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return qyProfitMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(QyProfit record) {
        return qyProfitMapper.insert(record);
    }

    @Override
    public int insertSelective(QyProfit record) {
        return qyProfitMapper.insertSelective(record);
    }

    @Override
    public QyProfit selectByPrimaryKey(String id) {
        return qyProfitMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(QyProfit record) {
        return qyProfitMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(QyProfit record) {
        return qyProfitMapper.updateByPrimaryKey(record);
    }

    @Override
    public QyProfit findByEnterpriseIdAndTime(String enterpriseId, Date time) {
        return qyProfitMapper.findByEnterpriseIdAndTime(enterpriseId, time);
    }

    @Override
    public DataGridResult getPageList(Query query) {

        Integer offset = (Integer) query.get("offset");
        Integer limit = (Integer) query.get("limit");
        String id = (String) query.get("id");
        List<QyProfit> rows =  qyProfitMapper.selectByEnterpriseId(offset, limit, id);
        Long total = qyProfitMapper.count(id);
        return new DataGridResult(total,rows);
    }

}
