package com.wsgxj.service.impl;

import com.wsgxj.domain.ZjIncome;
import com.wsgxj.mapper.ZjIncomeMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.wsgxj.service.ZjIncomeService;

import java.util.Date;

@Service
public class ZjIncomeServiceImpl implements ZjIncomeService{

    @Resource
    private ZjIncomeMapper zjIncomeMapper;

    @Override
    public int deleteByPrimaryKey(String id) {
        return zjIncomeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ZjIncome record) {
        return zjIncomeMapper.insert(record);
    }

    @Override
    public int insertSelective(ZjIncome record) {
        return zjIncomeMapper.insertSelective(record);
    }

    @Override
    public ZjIncome selectByPrimaryKey(String id) {
        return zjIncomeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ZjIncome record) {
        return zjIncomeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ZjIncome record) {
        return zjIncomeMapper.updateByPrimaryKey(record);
    }

    @Override
    public ZjIncome findByTownBackStreetsIdAndTime(String townBackStreetsId, Date time) {
        return zjIncomeMapper.findByTownBackStreetsIdAndTime(townBackStreetsId, time);
    }

}
