package com.wsgxj.service;

import com.wsgxj.domain.QyIncome;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

import java.util.Date;

public interface QyIncomeService{


    int deleteByPrimaryKey(String id);

    int insert(QyIncome record);

    int insertSelective(QyIncome record);

    QyIncome selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(QyIncome record);

    int updateByPrimaryKey(QyIncome record);

    int updateByTimeAndEnterpriseId( Date time, String enterpriseId, Double proportionIndustry);

    QyIncome findByEnterpriseIdAndTime(String enterpriseId, Date time);

    DataGridResult getPageList(Query query);
}
