package com.wsgxj.service;

import com.wsgxj.domain.ZjTownBackStreets;

import java.util.List;

public interface ZjTownBackStreetsService{


    int deleteByPrimaryKey(String id);

    int insert(ZjTownBackStreets record);

    int insertSelective(ZjTownBackStreets record);

    ZjTownBackStreets selectByPrimaryKey(String id);

    int updateByPrimaryKey(ZjTownBackStreets record);

    ZjTownBackStreets findByName(String name);

    List<ZjTownBackStreets> findAll();

}
