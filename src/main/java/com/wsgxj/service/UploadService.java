package com.wsgxj.service;

import java.util.List;
import java.util.Map;

public interface UploadService {
    String upload(Map<String,List<String[]>> datas) throws Exception;
}
