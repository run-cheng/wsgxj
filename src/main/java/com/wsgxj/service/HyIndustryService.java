package com.wsgxj.service;

import com.wsgxj.domain.HyIndustry;
import com.wsgxj.domain.sys.SysUser;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

import java.util.List;

public interface HyIndustryService{

    DataGridResult getPageList(Query query);

    void save(HyIndustry industry);

    HyIndustry getById(String id);

    void update(HyIndustry hyIndustry);
}
