package com.wsgxj.service;

import com.wsgxj.domain.ZjProfit;

import java.util.Date;

public interface ZjProfitService{


    int deleteByPrimaryKey(String id);

    int insert(ZjProfit record);

    int insertSelective(ZjProfit record);

    ZjProfit selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ZjProfit record);

    int updateByPrimaryKey(ZjProfit record);

    ZjProfit findByTownBackStreetsIdAndTime(String townBackStreetsId, Date time);

}
