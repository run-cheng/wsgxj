package com.wsgxj.service.sys.impl;

import com.wsgxj.domain.sys.SysUser;
import com.wsgxj.domain.sys.SysUserRole;
import com.wsgxj.mapper.sys.SysUserMapper;
import com.wsgxj.mapper.sys.SysUserRoleMapper;
import com.wsgxj.service.sys.SysUserService;
import com.wsgxj.util.PasswordUtil;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public DataGridResult getPageList(Query query) {

        Integer offset = (Integer) query.get("offset");
        Integer limit = (Integer) query.get("limit");
        Long createUserId = (Long) query.get("createUserId");
        String userName = (String) query.get("userName");

        //调用Dao查询分页列表数据
        List<SysUser> rows = sysUserMapper.find(offset, limit, createUserId, userName);
        for (SysUser sysUser : rows) {
            sysUser.setPassword(null);
        }

        //调用Dao查询总记录数
        Long total = sysUserMapper.count(createUserId, userName);

        //创建DataGridResult
        DataGridResult dataGridResult = new DataGridResult(total, rows);
        return dataGridResult;
    }

    @Override
    public void deleteBatch(Long[] userIds) {
        sysUserMapper.deleteBatch(userIds);
        sysUserRoleMapper.deleteByUserIds(userIds);
    }

    @Override
    public SysUser getById(Long userId) {
        SysUser user = sysUserMapper.selectByPrimaryKey(userId);
        user.setPassword(null);//不返回密码

        //查询用户所属的角色列表
        List<Long> roleList = sysUserRoleMapper.findRoleList(userId);
        user.setRoleIdList(roleList);
        if (roleList != null && roleList.size() != 0) {
            user.setRoleId(roleList.get(0));
        }
        return user;
    }

    @Override
    public void save(SysUser user) {

        //创建者id
        SysUser creator = (SysUser) SecurityUtils.getSubject().getPrincipal();
        user.setCreateUserId(creator.getUserId());

        user.setCreateTime(new Date());
        //md5加密
        user.setPassword(PasswordUtil.md5(user.getPassword(), user.getUsername()));
        sysUserMapper.addSysUser(user);

        //TODO 检查角色是否越权
        //checkRole(user);

        //先删除用户与角色关系
        Long userId = user.getUserId();
        Long[] userIdArr = new Long[]{userId};
        sysUserRoleMapper.deleteByUserIds(userIdArr);

        Long roleId = user.getRoleId();
        if (roleId == null) {
            return;
        }

        //保存用户与角色关系
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setRoleId(roleId);
        sysUserRole.setUserId(user.getUserId());
        sysUserRoleMapper.insert(sysUserRole);

    }

    @Override
    public void update(SysUser user) {
        //获取数据库中的密码
        String password = sysUserMapper.getPasswordById(user.getUserId());

        //如果用户没修改密码，则使用原来的密码
        if (StringUtils.isBlank(user.getPassword())) {
            user.setPassword(password);
        } else {
            user.setPassword(PasswordUtil.md5(user.getPassword(), user.getUsername()));
        }
        sysUserMapper.updateByPrimaryKey(user);

        //TODO 检查角色是否越权
        //checkRole(user);
        //先删除用户与角色关系
        Long userId = user.getUserId();
        sysUserRoleMapper.deleteByPrimaryKey(userId);


        if (user.getRoleId() != null) {
            SysUserRole sysUserRole = new SysUserRole();
            sysUserRole.setRoleId(user.getRoleId());
            sysUserRole.setUserId(user.getUserId());
            sysUserRoleMapper.insert(sysUserRole);
        }


        //List<Long> roleIdList = user.getRoleIdList();
        //if(roleIdList.size() == 0) {
        //	return ;
        //}

        ////保存用户与角色关系
        //for(int i=0; i<roleIdList.size(); i++) {
        //	SysUserRole sysUserRole = new SysUserRole();
        //	sysUserRole.setRoleId(roleIdList.get(i));
        //	sysUserRole.setUserId(user.getUserId());
        //	sysUserRoleMapper.insert(sysUserRole);
        //}
    }

    @Override
    public SysUser getByUsername(String username) {
        return sysUserMapper.getByUsername(username);
    }

    @Override
    public List<String> findRoleListByUserId(Long userId) {
        return sysUserRoleMapper.findRoleListByUserId(userId);
    }

}
