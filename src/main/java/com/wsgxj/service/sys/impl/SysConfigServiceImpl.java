package com.wsgxj.service.sys.impl;

import com.wsgxj.domain.sys.SysConfig;
import com.wsgxj.mapper.sys.SysConfigMapper;
import com.wsgxj.service.sys.SysConfigService;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SysConfigServiceImpl implements SysConfigService {
	
	@Autowired
	private SysConfigMapper sysConfigMapper;

	@Override
	public DataGridResult getPageList(Query query) {
		Integer offset = (Integer) query.get("offset");
		Integer limit = (Integer) query.get("limit");
		
		//调用dao查询分页数据
		List<SysConfig> rows = sysConfigMapper.find(offset, limit);
		//调用dao查询总记录数
		Long total = sysConfigMapper.count();

		DataGridResult result = new DataGridResult(total, rows);
		return result;
	}

	@Override
	public void deleteBatch(Long[] ids) {
        sysConfigMapper.deleteBatch(ids);
	}

	@Override
	public SysConfig getById(Long id) {
		return sysConfigMapper.selectByPrimaryKey(id);
	}

	@Override
	public void save(SysConfig sysConfig) {
		sysConfig.setStatus((byte)1);
		sysConfigMapper.insert(sysConfig);
	}

	@Override
	public void update(SysConfig sysConfig) {
		sysConfigMapper.updateByPrimaryKey(sysConfig);
	}

	@Override
	public List<SysConfig> findByKeyPrefix(String keyPrefix) {
		return sysConfigMapper.findByKeyPrefix(keyPrefix);
	}

}
