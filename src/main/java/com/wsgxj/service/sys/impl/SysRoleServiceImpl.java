package com.wsgxj.service.sys.impl;

import com.wsgxj.domain.sys.SysRole;
import com.wsgxj.domain.sys.SysRoleMenu;
import com.wsgxj.mapper.sys.SysRoleMapper;
import com.wsgxj.mapper.sys.SysRoleMenuMapper;
import com.wsgxj.mapper.sys.SysUserRoleMapper;
import com.wsgxj.service.sys.SysRoleService;
import com.wsgxj.util.ShiroUtil;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SysRoleServiceImpl implements SysRoleService {

	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
	private SysRoleMenuMapper sysRoleMenuMapper;
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	
	@Override
	public DataGridResult getPageList(Query query) {
		Integer offset = (Integer) query.get("offset");
		Integer limit = (Integer) query.get("limit");
        String roleName = (String) query.get("roleName");
		
		//调用Dao查询分页列表数据
		List<SysRole> rows = sysRoleMapper.find(offset, limit, roleName);

		//调用Dao查询总记录数
		Long total = sysRoleMapper.count(roleName);

		//创建DataGridResult
		DataGridResult dataGridResult = new DataGridResult(total, rows);
		return dataGridResult;
	}
	@Override
	public void deleteBatch(Long[] ids) {

		sysRoleMapper.deleteBatch(ids);
		sysRoleMenuMapper.deleteByRoleIds(ids);
		sysUserRoleMapper.deleteByRoleIds(ids);
	}
	@Override
	public SysRole getById(Long roleId) {
		SysRole role = sysRoleMapper.selectByPrimaryKey(roleId);
		
		//查询角色对应的菜单
		List<Long> menuIdList = sysRoleMenuMapper.findMenuIdList(roleId);
		role.setMenuIdList(menuIdList);
		
		return role;
	}
	@Override
	public void add(SysRole role) {
		role.setCreateTime(new Date());
        role.setCreateUserId(ShiroUtil.getUserId());
        sysRoleMapper.addSysRole(role);
		
		//先删除角色与菜单关系
        Long roleId = role.getRoleId();
        sysRoleMenuMapper.deleteByPrimaryKey(roleId);
		
		List<Long> menuIdList = role.getMenuIdList();
		if(menuIdList.size() == 0) {
			return ;
		}
		
		//保存角色与菜单关系
		for(int i=0; i < menuIdList.size(); i++) {
			SysRoleMenu sysRoleMenu = new SysRoleMenu();
			sysRoleMenu.setMenuId(menuIdList.get(i));
			sysRoleMenu.setRoleId(roleId);
			sysRoleMenuMapper.insert(sysRoleMenu);
		}
	}
	@Override
	public void update(SysRole role) {
		sysRoleMapper.updateByPrimaryKey(role);
		
		//先删除角色与菜单关系
		Long roleId = role.getRoleId();
		Long[] roleIdArr = new Long[]{roleId};
		sysRoleMenuMapper.deleteByRoleIds(roleIdArr);
		
		List<Long> menuIdList = role.getMenuIdList();
		if(menuIdList.size() == 0) {
			return ;
		}
		
		//保存角色与菜单关系
		for(int i=0; i<menuIdList.size(); i++) {
			SysRoleMenu sysRoleMenu = new SysRoleMenu();
			sysRoleMenu.setMenuId(menuIdList.get(i));
			sysRoleMenu.setRoleId(roleId);
			sysRoleMenuMapper.insert(sysRoleMenu);
		}
	}
	
	@Override
	public List<SysRole> findAll() {
		return sysRoleMapper.findAll();
	}
	@Override
	public List<String> selectRoleNameList(Long userId) {
		return sysRoleMapper.selectRoleNameList(userId);
	}

    @Override
    public List<Long> findMenuIdListByRoleId(Long roleId) {
        return sysRoleMenuMapper.findMenuIdListByRoleId(roleId);
    }


}
