package com.wsgxj.service.sys;


import com.wsgxj.domain.sys.SysUser;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

import java.util.List;

public interface SysUserService {

	DataGridResult getPageList(Query query);
	
	void deleteBatch(Long[] userIds);
	
	SysUser getById(Long roleId);
	
	void save(SysUser user);
	
	void update(SysUser user);
	
	SysUser getByUsername(String username);

    List<String> findRoleListByUserId(Long userId);
}
