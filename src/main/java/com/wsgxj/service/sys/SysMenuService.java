package com.wsgxj.service.sys;

import com.wsgxj.domain.sys.SysMenu;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

import java.util.List;

public interface SysMenuService {

	DataGridResult getPageList(Query query);
	
	void deleteBatch(Long[] ids);
	
	List<SysMenu> getNotButtonList();

	void save(SysMenu menu);
	
	SysMenu getById(Long menuId);
	
	void update(SysMenu menu);

	List<SysMenu> findAll();
	
	List<String> getUserPermsList(Long userId);

	List<SysMenu> getTopMenuList();
}
