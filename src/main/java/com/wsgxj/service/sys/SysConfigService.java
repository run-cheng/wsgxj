package com.wsgxj.service.sys;

import com.wsgxj.domain.sys.SysConfig;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

import java.util.List;

public interface SysConfigService {

	//分页业务方法
	DataGridResult getPageList(Query query);
	
	void deleteBatch(Long[] ids);
	
	SysConfig getById(Long id);
	
	void save(SysConfig sysConfig);
	
	void update(SysConfig sysConfig);
	
	List<SysConfig> findByKeyPrefix(String keyPrefix);
}
