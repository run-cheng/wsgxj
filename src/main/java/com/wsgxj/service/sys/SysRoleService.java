package com.wsgxj.service.sys;

import com.wsgxj.domain.sys.SysRole;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

import java.util.List;

public interface SysRoleService {

	DataGridResult getPageList(Query query);
	
	void deleteBatch(Long[] ids);
	
	SysRole getById(Long roleId);
	
	void add(SysRole role);
	
	void update(SysRole role);

	List<SysRole> findAll();

	List<String> selectRoleNameList(Long userId);

    List<Long> findMenuIdListByRoleId(Long roleId);
}
