package com.wsgxj.service;

import com.wsgxj.domain.QyEnterprise;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;

public interface QyEnterpriseService{


    int deleteByPrimaryKey(String id);

    int insert(QyEnterprise record);

    int insertSelective(QyEnterprise record);

    QyEnterprise selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(QyEnterprise record);

    int updateByPrimaryKey(QyEnterprise record);

    QyEnterprise findByName(String name);

    DataGridResult getPageList(Query query);

    QyEnterprise findById(String enterpriseId);

    void update(QyEnterprise qyEnterprise);

    void deleteBatch(String[] ids);

    DataGridResult findByIdAll(Query query);
}
