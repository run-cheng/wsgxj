package com.wsgxj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//扫描mybatis的mapper映射文件
@MapperScan(basePackages = "com.wsgxj.mapper")
@EnableTransactionManagement
public class WsgxjApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(WsgxjApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(WsgxjApplication.class);
    }
}
