package com.wsgxj.shiro;

import com.wsgxj.domain.sys.SysConfig;
import com.wsgxj.service.sys.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashMap;
import java.util.List;

public class FilterChainDefinitionMapBuilder {
	
	@Autowired
	private SysConfigService sysConfigService;

	public LinkedHashMap<String, String> buildFilter(){

		List<SysConfig> sysConfigList = sysConfigService.findByKeyPrefix("SHIRO_FILTER_CONFIG_");
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		for (SysConfig sysConfig : sysConfigList) {
			String value = sysConfig.getValue();
			String[] array = value.split("=");
			map.put(array[0], array[1]);
		}
		return map;
	}
}
