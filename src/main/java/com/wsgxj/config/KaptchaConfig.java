package com.wsgxj.config;

import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * google验证码配置bean
 */
@Configuration
public class KaptchaConfig {

    @Bean("producer")
    public Producer getProducer() {
        Properties kaptchaProperties = new Properties();
        //是否有边框 默认为true
        kaptchaProperties.put("kaptcha.border", "no");
        //验证码文本字符长度 默认为5
        kaptchaProperties.put("kaptcha.textproducer.char.length","4");
        //验证码图片高度 默认为50
        kaptchaProperties.put("kaptcha.image.height","50");
        //验证码图片宽度 默认为200
        kaptchaProperties.put("kaptcha.image.width","150");
        //验证码样式引擎 默认为WaterRipple
        kaptchaProperties.put("kaptcha.obscurificator.impl","com.google.code.kaptcha.impl.ShadowGimpy");
        //验证码文本字符颜色 默认为Color.BLACK
        kaptchaProperties.put("kaptcha.textproducer.font.color","black");
        //验证码文本字符大小 默认为40
        kaptchaProperties.put("kaptcha.textproducer.font.size","40");
        //验证码噪点生成对象 默认为DefaultNoise
        kaptchaProperties.put("kaptcha.noise.impl","com.google.code.kaptcha.impl.NoNoise");
        //kaptchaProperties.put("kaptcha.noise.impl","com.google.code.kaptcha.impl.DefaultNoise");
        //验证码文本字符内容范围 默认为abcde2345678gfynmnpwx
        kaptchaProperties.put("kaptcha.textproducer.char.string","acdefhkmnprtwxy2345678");

        Config config = new Config(kaptchaProperties);
        return config.getProducerImpl();
    }
}
