package com.wsgxj.config;

import com.wsgxj.shiro.FilterChainDefinitionMapBuilder;
import com.wsgxj.shiro.UserRealm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;

/**
 * shiro配置bean
 */
@Configuration
public class ShiroConfig {

    /**
     * 自定义realm
     * @return
     */
    @Bean("userRealm")
    public UserRealm userRealm(){
        return new UserRealm();
    }

    /**
     * securityManager
     * @return
     */
    @Bean("securityManager")
    public DefaultWebSecurityManager securityManager(){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm());
        securityManager.setRememberMeManager(rememberMeManager());
        return securityManager;
    }

    /**
     * cookie管理对象;记住我功能
     * @return
     */
    public CookieRememberMeManager rememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCookie(rememberMeCookie());
        return cookieRememberMeManager;
    }

    /**
     * cookie对象;
     * @return
     */
    public SimpleCookie rememberMeCookie() {
        // 这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        // <!-- 记住我cookie生效时间30天 ,单位秒;-->
        simpleCookie.setMaxAge(2592000);
        return simpleCookie;
    }

    /**
     * shiro主过滤器
     * @return
     */
    @Bean("shiroFilter")
    public ShiroFilterFactoryBean shiroFilter(){
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
        //shiro核心安全接口，必须配置
        shiroFilter.setSecurityManager(securityManager());
        //登陆时的链接
        shiroFilter.setLoginUrl("/login.jsp");
        //登陆成功后的链接
        shiroFilter.setSuccessUrl("/index");
        //用户访问未授权的资源时跳转的链接
        shiroFilter.setUnauthorizedUrl("/unauthorized.jsp");
        //过滤器链
        shiroFilter.setFilterChainDefinitionMap(filterChainDefinitionMap());
        return shiroFilter;
    }

    @Bean("filterChainDefinitionMapBuilder")
    public FilterChainDefinitionMapBuilder filterChainDefinitionMapBuilder(){
        return new FilterChainDefinitionMapBuilder();
    }

    @Bean("filterChainDefinitionMap")
    public LinkedHashMap<String, String> filterChainDefinitionMap(){
        return filterChainDefinitionMapBuilder().buildFilter();
    }

    /**
     *  开启Shiro的注解(如@RequiresRoles,@RequiresPermissions)
     * @return
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }

    /**
     * 开启aop注解支持
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor() {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager());
        return authorizationAttributeSourceAdvisor;
    }
}
