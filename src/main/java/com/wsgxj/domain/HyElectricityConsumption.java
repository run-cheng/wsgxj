package com.wsgxj.domain;

import java.util.Date;

public class HyElectricityConsumption {
    /**
    * ID
    */
    private String id;

    /**
    * 用户个数
    */
    private Integer numberUsers;

    /**
    * 装接容量（千瓦）
    */
    private Integer assemblingCapacity;

    /**
    * 本月用电量（万千瓦时）
    */
    private Double thisMonthElectricity;

    /**
    * 用电量上年同月（万千瓦时）
    */
    private Double lastYearSameMonthElectricity;

    /**
    * 本月同比
    */
    private Double monthIncrease;

    /**
    * 用电量累计（万千瓦时）
    */
    private Double thisYearGrandTotal;

    /**
    * 用电量上年累计（万千瓦时）
    */
    private Double lastYearGrandTotal;

    /**
    * 本年与上年增减值
    */
    private Double yearChange;

    /**
    * 累计同比
    */
    private Double yearIncrease;

    /**
    * 累计全县比重
    */
    private String proportionTheCounty;

    /**
    * 本月同比波动分析
    */
    private Double monthIncreaseAnalysis;

    /**
    * 单位
    */
    private String unit;

    /**
    * 口径
    */
    private String caliber;


    /**
    * 审核状态
    */
    private String reviewStatus;

    /**
    * 月份
    */
    private Date time;

    /**
    * 关联行业分类外键
    */
    private String industryId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNumberUsers() {
        return numberUsers;
    }

    public void setNumberUsers(Integer numberUsers) {
        this.numberUsers = numberUsers;
    }

    public Integer getAssemblingCapacity() {
        return assemblingCapacity;
    }

    public void setAssemblingCapacity(Integer assemblingCapacity) {
        this.assemblingCapacity = assemblingCapacity;
    }

    public Double getThisMonthElectricity() {
        return thisMonthElectricity;
    }

    public void setThisMonthElectricity(Double thisMonthElectricity) {
        this.thisMonthElectricity = thisMonthElectricity;
    }

    public Double getLastYearSameMonthElectricity() {
        return lastYearSameMonthElectricity;
    }

    public void setLastYearSameMonthElectricity(Double lastYearSameMonthElectricity) {
        this.lastYearSameMonthElectricity = lastYearSameMonthElectricity;
    }

    public Double getMonthIncrease() {
        return monthIncrease;
    }

    public void setMonthIncrease(Double monthIncrease) {
        this.monthIncrease = monthIncrease;
    }

    public Double getThisYearGrandTotal() {
        return thisYearGrandTotal;
    }

    public void setThisYearGrandTotal(Double thisYearGrandTotal) {
        this.thisYearGrandTotal = thisYearGrandTotal;
    }

    public Double getLastYearGrandTotal() {
        return lastYearGrandTotal;
    }

    public void setLastYearGrandTotal(Double lastYearGrandTotal) {
        this.lastYearGrandTotal = lastYearGrandTotal;
    }

    public Double getYearChange() {
        return yearChange;
    }

    public void setYearChange(Double yearChange) {
        this.yearChange = yearChange;
    }

    public Double getYearIncrease() {
        return yearIncrease;
    }

    public void setYearIncrease(Double yearIncrease) {
        this.yearIncrease = yearIncrease;
    }

    public String getProportionTheCounty() {
        return proportionTheCounty;
    }

    public void setProportionTheCounty(String proportionTheCounty) {
        this.proportionTheCounty = proportionTheCounty;
    }

    public Double getMonthIncreaseAnalysis() {
        return monthIncreaseAnalysis;
    }

    public void setMonthIncreaseAnalysis(Double monthIncreaseAnalysis) {
        this.monthIncreaseAnalysis = monthIncreaseAnalysis;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCaliber() {
        return caliber;
    }

    public void setCaliber(String caliber) {
        this.caliber = caliber;
    }

    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }
}