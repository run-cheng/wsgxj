package com.wsgxj.domain;

import java.util.Date;

/**
    * qy_income
    */
public class QyIncome {
    /**
    * 营业收入ID
    */
    private String id;

    /**
    * 本月收入
    */
    private Double thisMonthIncome;

    /**
    * 去年同期收入
    */
        private Double lastYearSameMonthIncome;

    /**
    * 本月对比去年同期增幅
    */
    private Double monthIncrease;

    /**
    * 本年累计
    */
    private Double thisYearGrandTotal;

    /**
    * 行业比重
    */
    private Double proportionIndustry;

    /**
    * 去年累计
    */
    private Double lastYearGrandTotal;

    /**
    * 本年累计对比去年同期增幅
    */
    private Double yearIncrease;

    /**
    * 月份
    */
    private Date time;

    /**
    * 关联企业外键
    */
    private String enterpriseId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getThisMonthIncome() {
        return thisMonthIncome;
    }

    public void setThisMonthIncome(Double thisMonthIncome) {
        this.thisMonthIncome = thisMonthIncome;
    }

    public Double getLastYearSameMonthIncome() {
        return lastYearSameMonthIncome;
    }

    public void setLastYearSameMonthIncome(Double lastYearSameMonthIncome) {
        this.lastYearSameMonthIncome = lastYearSameMonthIncome;
    }

    public Double getMonthIncrease() {
        return monthIncrease;
    }

    public void setMonthIncrease(Double monthIncrease) {
        this.monthIncrease = monthIncrease;
    }

    public Double getThisYearGrandTotal() {
        return thisYearGrandTotal;
    }

    public void setThisYearGrandTotal(Double thisYearGrandTotal) {
        this.thisYearGrandTotal = thisYearGrandTotal;
    }

    public Double getProportionIndustry() {
        return proportionIndustry;
    }

    public void setProportionIndustry(Double proportionIndustry) {
        this.proportionIndustry = proportionIndustry;
    }

    public Double getLastYearGrandTotal() {
        return lastYearGrandTotal;
    }

    public void setLastYearGrandTotal(Double lastYearGrandTotal) {
        this.lastYearGrandTotal = lastYearGrandTotal;
    }

    public Double getYearIncrease() {
        return yearIncrease;
    }

    public void setYearIncrease(Double yearIncrease) {
        this.yearIncrease = yearIncrease;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(String enterpriseId) {
        this.enterpriseId = enterpriseId;
    }
}