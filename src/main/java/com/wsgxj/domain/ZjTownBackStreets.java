package com.wsgxj.domain;

import java.util.List;

/**
 * zj_town_back_streets
 */
public class ZjTownBackStreets {
    private static final long serialVersionUID = 1L;
    /**
     * 镇街ID
     */
    private String id;

    /**
     * 镇街名称
     */
    private String name;

    /**
     * 营业收入
     */
    private List<ZjIncome> zjIncomes;

    /**
     * 利润总额
     */
    private List<ZjProfit> zjProfits;
    /**
     * 企业信息
     */
    private List<QyEnterprise> qyEnterprises;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ZjIncome> getZjIncomes() {
        return zjIncomes;
    }

    public List<ZjProfit> getZjProfits() {
        return zjProfits;
    }

    public List<QyEnterprise> getQyEnterprises() {
        return qyEnterprises;
    }

    public void setZjIncomes(List<ZjIncome> zjIncomes) {
        this.zjIncomes = zjIncomes;
    }

    public void setZjProfits(List<ZjProfit> zjProfits) {
        this.zjProfits = zjProfits;
    }

    public void setQyEnterprises(List<QyEnterprise> qyEnterprises) {
        this.qyEnterprises = qyEnterprises;
    }
}