package com.wsgxj.domain;

import java.util.List;

public class HyIndustry {
    /**
    * 行业分类ID
    */
    private String id;

    /**
    * 行业名称
    */
    private String name;

    /**
     * 行业编码
     */
    private String industryCode;

    /**
    * 分类外键
    */
    private String industryId;

    /**
     * 用电量
     */
    private List<HyElectricityConsumption> hyElectricityConsumptions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId;
    }

    public List<HyElectricityConsumption> getHyElectricityConsumptions() {
        return hyElectricityConsumptions;
    }

    public void setHyElectricityConsumptions(List<HyElectricityConsumption> hyElectricityConsumptions) {
        this.hyElectricityConsumptions = hyElectricityConsumptions;
    }
}