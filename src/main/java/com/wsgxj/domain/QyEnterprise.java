package com.wsgxj.domain;

import java.util.Date;
import java.util.List;

/**
 * qy_enterprise
 */
public class QyEnterprise {
    /**
     * 企业ID
     */
    private String id;

    /**
     * 企业名称
     */
    private String name;

    /**
     * 行业代码
     */
    private String industryCode;

    /**
     * 创建时间
     */
    private Date creationTime;

    /**
     * 创建人ID
     */
    private Long creationUserId;

    /**
     * 逻辑删除字段，删除：1  未删除：0
     */
    private Integer deleteId;

    /**
     * 逻辑删除时间
     */
    private Date deleteTime;

    /**
     * 关联镇街表外键
     */
    private String zjTownBackStreetsId;

    /**
     * 营业收入
     */
    private List<QyIncome> qyIncomes;

    /**
     * 利润总额
     */
    private List<QyProfit> qyProfits;

    /**
     * 镇街名称
     */
    private String zjName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Long getCreationUserId() {
        return creationUserId;
    }

    public void setCreationUserId(Long creationUserId) {
        this.creationUserId = creationUserId;
    }

    public Integer getDeleteId() {
        return deleteId;
    }

    public void setDeleteId(Integer deleteId) {
        this.deleteId = deleteId;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getZjTownBackStreetsId() {
        return zjTownBackStreetsId;
    }

    public void setZjTownBackStreetsId(String zjTownBackStreetsId) {
        this.zjTownBackStreetsId = zjTownBackStreetsId;
    }

    public List<QyIncome> getQyIncomes() {
        return qyIncomes;
    }

    public List<QyProfit> getQyProfits() {
        return qyProfits;
    }

    public void setQyIncomes(List<QyIncome> qyIncomes) {
        this.qyIncomes = qyIncomes;
    }

    public void setQyProfits(List<QyProfit> qyProfits) {
        this.qyProfits = qyProfits;
    }

    public String getZjName() {
        return zjName;
    }

    public void setZjName(String zjName) {
        this.zjName = zjName;
    }
}