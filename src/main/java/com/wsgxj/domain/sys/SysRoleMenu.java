package com.wsgxj.domain.sys;

import java.io.Serializable;

/**
 * 系统角色与系统菜单关联表
 */
public class SysRoleMenu implements Serializable {
    //系统主键
    private Long id;
    //角色id
    private Long roleId;
    //菜单id
    private Long menuId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }
}