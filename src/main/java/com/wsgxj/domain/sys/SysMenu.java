package com.wsgxj.domain.sys;

import java.io.Serializable;
import java.util.List;

/**
 * 系统菜单表
 */
public class SysMenu implements Serializable {
    //系统主键
    private Long menuId;

    //菜单名称
    private String name;
    //菜单url
    private String url;
    //菜单授权（多个用逗号分隔，如：user:list,user:create)
    private String perms;
    //类型 0：目录	1：菜单	2：按钮
    private Integer type;
    //菜单图标
    private String icon;
    //排序号
    private Integer orderNum;

    //上级菜单
    private SysMenu parentMenu;
    //下级菜单
    private List<SysMenu> childrenMenu;
    private Long children;

    //树状结构展示所需字段
    private Long parentId;

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public SysMenu getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(SysMenu parentMenu) {
        this.parentMenu = parentMenu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public List<SysMenu> getChildrenMenu() {
        return childrenMenu;
    }

    public void setChildrenMenu(List<SysMenu> childrenMenu) {
        this.childrenMenu = childrenMenu;
    }

    public Long getChildren() {
        return children;
    }

    public void setChildren(Long children) {
        this.children = children;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}