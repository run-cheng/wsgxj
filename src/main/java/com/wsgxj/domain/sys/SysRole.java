package com.wsgxj.domain.sys;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统角色表
 */
public class SysRole implements Serializable {
    //系统主键
    private Long roleId;
    //角色名称
    private String roleName;
    //备注
    private String remark;
    //创建者ID
    private Long createUserId;
    //创建时间
    private Date createTime;

    //页面展示所需字段
    private List<Long> menuIdList;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<Long> getMenuIdList() {
        return menuIdList;
    }

    public void setMenuIdList(List<Long> menuIdList) {
        this.menuIdList = menuIdList;
    }
}