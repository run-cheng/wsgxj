package com.wsgxj.domain.sys;

import java.io.Serializable;

/**
 * 系统用户与系统角色关联表
 */
public class SysUserRole implements Serializable {
    //系统主键
    private Long id;
    //用户id
    private Long userId;
    //角色id
    private Long roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}