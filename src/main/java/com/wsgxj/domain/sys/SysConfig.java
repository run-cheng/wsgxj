package com.wsgxj.domain.sys;

import java.io.Serializable;

/**
 * 系统配置表
 */
public class SysConfig implements Serializable {
    //系统主键
    private Long id;
    //key
    private String key;
    //value
    private String value;
    //状态 0:隐藏 1:显示
    private Byte status;
    //备注
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}