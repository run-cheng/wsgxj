package com.wsgxj.domain;

import java.util.Date;

/**
    * zj_profit
    */
public class ZjProfit {
    /**
    * 利润总额ID
    */
    private String id;

    /**
    * 本月利润
    */
    private Double thisMonthProfit;

    /**
    * 去年同期利润
    */
    private Double lastYearSameMonthProfit;

    /**
    * 本月对比去年同期增幅
    */
    private Double monthIncrease;

    /**
    * 本年累计
    */
    private Double thisYearGrandTotal;

    /**
    * 全县比重
    */
    private Double proportionTheCounty;

    /**
    * 去年累计
    */
    private Double lastYearGrandTotal;

    /**
    * 本年累计对比去年同期增幅
    */
    private Double yearIncrease;

    /**
    * 增幅位次
    */
    private Integer rank;

    /**
    * 月份
    */
    private Date time;

    /**
    * 关联镇街外键
    */
    private String townBackStreetsId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getThisMonthProfit() {
        return thisMonthProfit;
    }

    public void setThisMonthProfit(Double thisMonthProfit) {
        this.thisMonthProfit = thisMonthProfit;
    }

    public Double getLastYearSameMonthProfit() {
        return lastYearSameMonthProfit;
    }

    public void setLastYearSameMonthProfit(Double lastYearSameMonthProfit) {
        this.lastYearSameMonthProfit = lastYearSameMonthProfit;
    }

    public Double getMonthIncrease() {
        return monthIncrease;
    }

    public void setMonthIncrease(Double monthIncrease) {
        this.monthIncrease = monthIncrease;
    }

    public Double getThisYearGrandTotal() {
        return thisYearGrandTotal;
    }

    public void setThisYearGrandTotal(Double thisYearGrandTotal) {
        this.thisYearGrandTotal = thisYearGrandTotal;
    }

    public Double getProportionTheCounty() {
        return proportionTheCounty;
    }

    public void setProportionTheCounty(Double proportionTheCounty) {
        this.proportionTheCounty = proportionTheCounty;
    }

    public Double getLastYearGrandTotal() {
        return lastYearGrandTotal;
    }

    public void setLastYearGrandTotal(Double lastYearGrandTotal) {
        this.lastYearGrandTotal = lastYearGrandTotal;
    }

    public Double getYearIncrease() {
        return yearIncrease;
    }

    public void setYearIncrease(Double yearIncrease) {
        this.yearIncrease = yearIncrease;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTownBackStreetsId() {
        return townBackStreetsId;
    }

    public void setTownBackStreetsId(String townBackStreetsId) {
        this.townBackStreetsId = townBackStreetsId;
    }
}