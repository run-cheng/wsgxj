package com.wsgxj.controller.base;

import com.wsgxj.constant.SysConstant;
import com.wsgxj.domain.QyEnterprise;
import com.wsgxj.domain.ZjTownBackStreets;
import com.wsgxj.domain.sys.SysRole;
import com.wsgxj.domain.sys.SysUser;
import com.wsgxj.service.QyEnterpriseService;
import com.wsgxj.service.QyIncomeService;
import com.wsgxj.service.QyProfitService;
import com.wsgxj.service.ZjTownBackStreetsService;
import com.wsgxj.util.ShiroUtil;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import com.wsgxj.vo.R;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/base/enterprise")
public class BaseEnterpriseController {

    //企业信息
    @Autowired
    private QyEnterpriseService qyEnterpriseService;
    //企业营收
    @Autowired
    private QyIncomeService qyIncomeService;
    //企业利润
    @Autowired
    private QyProfitService qyProfitService;
    //镇街信息
    @Autowired
    private ZjTownBackStreetsService zjTownBackStreetsService;

    @RequestMapping("/{page}")
    public String listPage(@PathVariable String page, HttpServletRequest request) {
        //新增时初始化镇街
        List<ZjTownBackStreets> zjList = zjTownBackStreetsService.findAll();
        request.setAttribute("zjList",zjList);
        return "base/enterprise/" + page;
    }

    //企业列表
    @RequestMapping("/list")
    @ResponseBody
    @RequiresPermissions({"base:enterprise:list"})
    public DataGridResult list(@RequestParam Map<String, Object> params) {

        //查询数据列表
        Query query = new Query(params);
        DataGridResult dgr = qyEnterpriseService.getPageList(query);
        return dgr;
    }

    //企业新增
    @RequestMapping("/add")
    @ResponseBody
    @RequiresPermissions({"base:enterprise:add"})
    public R add(@RequestBody QyEnterprise enterprise) {
        try {
            qyEnterpriseService.insertSelective(enterprise);
            return R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("必要的属性未添加");
        }
    }

    //企业修改查询数据
    @RequestMapping("/info/{enterpriseId}")
    @ResponseBody
    @RequiresPermissions({"base:enterprise:info"})
    public R info(@PathVariable String enterpriseId) {
        QyEnterprise qyEnterprise = qyEnterpriseService.findById(enterpriseId);
        return R.ok().put("enterprise", qyEnterprise);
    }

    //企业修改保存
    @RequestMapping("/update")
    @ResponseBody
    @RequiresPermissions({"base:enterprise:update"})
    public R update(@RequestBody QyEnterprise qyEnterprise) {
        qyEnterpriseService.update(qyEnterprise);
        return R.ok();
    }

    //企业逻辑删除
    @RequestMapping("/delete")
    @ResponseBody
    @RequiresPermissions({"base:enterprise:delete"})
    public R deleteMore(@RequestBody String[] ids) {
        qyEnterpriseService.deleteBatch(ids);
        return R.ok();
    }

  /*  //营收和利润数据
    @RequestMapping("/full")
    @ResponseBody
    public DataGridResult full(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        DataGridResult dgr = qyEnterpriseService.findByIdAll(query);
        return dgr;
    }
    */

  //利润数据集合
    @RequestMapping("/lrList")
    @ResponseBody
    @RequiresPermissions({"base:enterprise:view"})
    public DataGridResult lrList(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        DataGridResult dgr = qyProfitService.getPageList(query);
        return dgr;
    }

    //营收数据集合
    @RequestMapping("/ysList")
    @ResponseBody
    @RequiresPermissions({"base:enterprise:view"})
    public DataGridResult ysList(@RequestParam Map<String, Object> params) {
        Query query = new Query(params);
        DataGridResult dgr = qyIncomeService.getPageList(query);
        return dgr;
    }


}
