package com.wsgxj.controller.base;

import com.wsgxj.constant.SysConstant;
import com.wsgxj.domain.HyIndustry;
import com.wsgxj.domain.sys.SysRole;
import com.wsgxj.domain.sys.SysUser;
import com.wsgxj.mapper.HyIndustryMapper;
import com.wsgxj.service.HyElectricityConsumptionService;
import com.wsgxj.service.HyIndustryService;
import com.wsgxj.util.ShiroUtil;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import com.wsgxj.vo.R;
import org.apache.commons.lang.ArrayUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/base/industry")
public class BaseIndustryController {
    //行业分类
    @Autowired
    private HyIndustryService hyIndustryService;
    //用电量
    @Autowired
    private HyElectricityConsumptionService hyElectricityConsumptionService;

    @RequestMapping("/{page}")
    public String listPage(@PathVariable String page, HttpServletRequest request) {
        return "base/industry/" + page;
    }

    //行业列表
    @RequestMapping("/list")
    @ResponseBody
    @RequiresPermissions({"base:industry:list"})
    public DataGridResult list(@RequestParam Map<String, Object> params) {

        //查询数据列表
        Query query = new Query(params);
        DataGridResult dgr = hyIndustryService.getPageList(query);
        return dgr;
    }

    @RequestMapping("/full")
    @ResponseBody
    @RequiresPermissions({"base:industry:view"})
    public DataGridResult full(@RequestParam Map<String,Object> params){
        //查询用电数据列表
        Query query = new Query(params);
        DataGridResult dgr = hyElectricityConsumptionService.getPageList(query);
        return dgr;
    }
    //行业新增
    @RequestMapping("/add")
    @ResponseBody
    @RequiresPermissions({"base:industry:add"})
    public R add(@RequestBody HyIndustry industry) {
        try {
            hyIndustryService.save(industry);
            return R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error("必要的属性未添加");
        }
    }

    //行业修改查询数据
    @RequestMapping("/info/{userId}")
    @ResponseBody
    @RequiresPermissions({"base:industry:info"})
    public R info(@PathVariable String userId) {
        HyIndustry hyIndustry = hyIndustryService.getById(userId);
        return R.ok().put("industry", hyIndustry);
    }

    //行业修改保存
    @RequestMapping("/update")
    @ResponseBody
    @RequiresPermissions({"base:industry:update"})
    public R update(@RequestBody HyIndustry hyIndustry) {
        hyIndustryService.update(hyIndustry);
        return R.ok();
    }


    ////行业删除
    //@RequestMapping("/delete")
    //@ResponseBody
    //@RequiresPermissions({"base:industry:delete"})
    //public R deleteMore(@RequestBody Long[] ids) {
    //
    //    //系统管理员不能删除
    //    if(ArrayUtils.contains(ids, SysConstant.ADMIN_ID)) {
    //        return R.error("系统管理员不能删除");
    //    }
    //
    //    //当前用户不能删除
    //    if(ArrayUtils.contains(ids, ShiroUtil.getUserId())) { return R.error("当前用户不能删除"); }
    //
    //    sysUserService.deleteBatch(ids);
    //    return R.ok();
    //}
}
