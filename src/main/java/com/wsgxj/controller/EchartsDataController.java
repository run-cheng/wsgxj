package com.wsgxj.controller;

import com.wsgxj.service.EchartsDataService;
import com.wsgxj.vo.EchartsDataResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/echartsData")
public class EchartsDataController {

    @Autowired
    private EchartsDataService echartsDataService;

    @RequestMapping("/getThreeMajorCategories")
    public List<EchartsDataResult> getThreeMajorCategories(){
        return echartsDataService.getThreeMajorCategories();
    }

    @RequestMapping("/getKeyIndustries")
    public List<EchartsDataResult> getKeyIndustries(){
        return echartsDataService.getKeyIndustries();
    }

    @RequestMapping("/getEnterpriseNumber")
    public Map<String,List> getEnterpriseNumber(Integer year){
        return echartsDataService.getEnterpriseNumber(year);
    }

    @RequestMapping("/getProfitGrowth")
    public Map<String,List> getProfitGrowth(String start,String end){
        return echartsDataService.getProfitGrowth(start,end);
    }

    //汶上县规上工业企业营收累计增幅
    @RequestMapping("/getRevenueCumulativeGrowth")
    public Map<String,List> getRevenueCumulativeGrowth(){
        return echartsDataService.getRevenueCumulativeGrowth();
    }

    //各镇街营业收入增幅
    @RequestMapping("/getRevenueGrowth")
    public Map<String,List> getRevenueGrowth(String start,String end){
        return echartsDataService.getRevenueGrowth(start,end);
    }
    //汶上县规上工业企业利润累计增幅
    @RequestMapping("/getProfitGrowthHas")
    public Map<String,List> getProfitGrowthHas(){
        return echartsDataService.getProfitGrowthHas();
    }

    //各镇街营收占全县比重
    @RequestMapping("/getRevenueProportionTheCounty")
    public List<EchartsDataResult> getRevenueProportionTheCounty(String start,String end){
        return echartsDataService.getRevenueProportionTheCounty(start,end);
    }
}
