package com.wsgxj.controller;

import com.wsgxj.util.DateUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/echarts")
public class EchartsController {

    //三大门类规上工业企业分布
    @RequestMapping("/enterprisesLocationByCategoryAndTime")
    public String enterprisesLocationByCategoryAndTime() {

        return "echarts/EnterprisesLocationByCategoryAndTime";
    }

    //重点行业规上工业企业个数
    @RequestMapping("/keyIndustries")
    public String keyIndustries(){
        return "echarts/KeyIndustries";
    }

    //规上工业企业个数
    @RequestMapping("/enterpriseNumber")
    public String enterpriseNumber(HttpServletRequest request){
        int year = DateUtil.getYear();
        request.setAttribute("year",year);
        return "echarts/EnterpriseNumber";
    }

    //各镇街营业收入增幅
    @RequestMapping("/revenueGrowth")
    public String revenueGrowth(){
        return "echarts/RevenueGrowth";
    }

    //各镇街营收占全县比重
    @RequestMapping("/revenueProportionTheCounty")
    public String revenueProportionTheCounty(){
        return "echarts/RevenueProportionTheCounty";
    }

    //各镇街利润增幅
    @RequestMapping("/profitGrowth")
    public String profitGrowth(){
        return "echarts/ProfitGrowth";
    }

    //汶上县规上工业企业营收累计增幅
    @RequestMapping("/revenueCumulativeGrowth")
    public String revenueCumulativeGrowth(){
        return "echarts/RevenueCumulativeGrowth";
    }

    //汶上县规上工业企业利润累计增幅
    @RequestMapping("/profitGrowthHas")
    public String profitGrowthHas() {
        return "echarts/ProfitGrowthHas";
    }


}
