package com.wsgxj.controller;

import com.wsgxj.service.UploadService;
import com.wsgxj.service.impl.UploadServiceImpl;
import com.wsgxj.util.POIUtils;
import com.wsgxj.util.TemplateUtil;
import com.wsgxj.vo.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UploadController {


    @Autowired
    private UploadService uploadService;

    @RequestMapping("/toUpload")
    public String toUpload() {
        System.out.println("上传跳转成功");
        return "jsp/upload";
    }

    @RequestMapping("/upload")
    @ResponseBody
    public Map upload(@RequestParam("file")MultipartFile file) throws Exception {
        try {
            Map<String, List<String[]>> datas = POIUtils.readExcel(file);
            String message = uploadService.upload(datas);
            return R.ok(message);
        } catch (Exception e) {
            e.printStackTrace();
            HashMap<String, Object> errorMessage = new HashMap<>();
            errorMessage.put("error",e.getMessage());
            return errorMessage;
        }
    }

    @RequestMapping("getElectricityTemplate")
    public void getElectricityTemplate(HttpServletResponse response){
        TemplateUtil.getTemplate(response,"行业用电分类（电网）_国网汶上县供电公司");
    }

    @RequestMapping("getEnterpriseTemplate")
    public void getEnterpriseTemplate(HttpServletResponse response){
        TemplateUtil.getTemplate(response,"3月份规模以上工业企业经济指标情况统计表汇总");
    }
}
