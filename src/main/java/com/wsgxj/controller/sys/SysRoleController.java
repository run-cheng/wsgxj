package com.wsgxj.controller.sys;

import com.wsgxj.domain.sys.SysRole;
import com.wsgxj.service.sys.SysRoleService;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import com.wsgxj.vo.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sys/role")
public class SysRoleController {

	@Autowired
	private SysRoleService sysRoleService;

	@RequestMapping("/{page}")
	public String listPage(@PathVariable String page) {
		return "sys/role/" + page; 
	}
	
	@RequestMapping("/list")
	@ResponseBody
	@RequiresPermissions({"sys:role:list"})
	public DataGridResult getPage(@RequestParam Map<String, Object> params) {
		//查询数据列表
		Query query = new Query(params);
		DataGridResult dgr = sysRoleService.getPageList(query);
		return dgr;
	}

    @RequestMapping("/add")
    @ResponseBody
    @RequiresPermissions({"sys:role:add"})
    public R add(@RequestBody SysRole role) {
        sysRoleService.add(role);
        return R.ok();
    }
	
	@RequestMapping("/info/{roleId}")
	@ResponseBody
	@RequiresPermissions({"sys:role:info"})
	public R info(@PathVariable Long roleId) {
		SysRole sysRole = sysRoleService.getById(roleId);
		return R.ok().put("role", sysRole);
	}

    //获取所有选中的menu节点id
    @RequestMapping("/findMenuIdListByRoleId/{roleId}")
    @ResponseBody
    @RequiresPermissions({"sys:role:findMenuIdListByRoleId"})
    public R findMenuIdListByRoleId(@PathVariable("roleId") Long roleId) {
        List<Long> menuIdList = sysRoleService.findMenuIdListByRoleId(roleId);
        return R.ok().put("menuIdList", menuIdList);
    }
	
	@RequestMapping("/update")
	@ResponseBody
	@RequiresPermissions({"sys:role:update"})
	public R update(@RequestBody SysRole role) {
		sysRoleService.update(role);
		return R.ok();
	}

    @RequestMapping("/delete")
    @ResponseBody
    @RequiresPermissions({"sys:role:delete"})
    public R deleteMore(@RequestBody Long[] ids) {
		int i = Arrays.asList(ids).indexOf(1L);
		if (i != -1){
			return R.error("管理员角色禁止删除");
		}
		sysRoleService.deleteBatch(ids);
        return R.ok();
    }
	
	//角色列表
	/*@RequestMapping("/select")
	@ResponseBody
	@RequiresPermissions({"sys:role:select"})
	public R select() {
		//如果不是超级管理员，则只查询自己所拥有的角色列表
		*//*
		 * if(getUserId() != Constant.SUPER_ADMIN) { map.put("createUserId",
		 * getUserId()); }
		 *//*
		List<SysRole> roleList = sysRoleService.findAll();
		return R.ok().put("roleList", roleList);
	}*/
}
