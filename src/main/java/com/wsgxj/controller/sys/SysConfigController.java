package com.wsgxj.controller.sys;

import com.wsgxj.domain.sys.SysConfig;
import com.wsgxj.service.sys.SysConfigService;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import com.wsgxj.vo.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


//定时任务
@Controller
@RequestMapping("/sys/config")
public class SysConfigController {

	@Autowired
	private SysConfigService sysConfigService;
	
	@RequestMapping("/{page}") 
	public String index(@PathVariable String page) {
		return "sys/config/" + page; 
	}
	
	@RequestMapping("/list")
	@ResponseBody
	@RequiresPermissions({"sys:config:list"})
	public DataGridResult getPage(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		DataGridResult dgr = sysConfigService.getPageList(query);
		return dgr;
	}

    @RequestMapping("/save")
    @ResponseBody
    @RequiresPermissions({"sys:config:save"})
    public R save(@RequestBody SysConfig sysConfig) {
        sysConfigService.save(sysConfig);
        return R.ok();
    }
	
	@RequestMapping("/delete")
	@ResponseBody
	@RequiresPermissions({"sys:config:delete"})
	public R deleteBatch(@RequestBody Long[] ids) {
		sysConfigService.deleteBatch(ids);
		return R.ok();
	}

    @RequestMapping("/update")
    @ResponseBody
    @RequiresPermissions({"sys:config:update"})
    public R update(@RequestBody SysConfig sysConfig) {
        sysConfigService.update(sysConfig);
        return R.ok();
    }

	@RequestMapping("/info/{id}")
	@ResponseBody
	@RequiresPermissions({"sys:config:info"})
	public R info(@PathVariable Long id) {
		SysConfig sysConfig = sysConfigService.getById(id);
		return R.ok().put("config", sysConfig);
	}

}
