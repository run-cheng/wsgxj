package com.wsgxj.controller.sys;

import com.wsgxj.domain.sys.SysMenu;
import com.wsgxj.service.sys.SysMenuService;
import com.wsgxj.vo.DataGridResult;
import com.wsgxj.vo.Query;
import com.wsgxj.vo.R;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sys/menu")
public class SysMenuController {

	@Autowired
	private SysMenuService sysMenuService;
	
	@RequestMapping("/{page}")
	public String index(@PathVariable String page) {
		return "sys/menu/" + page; 
	}
	 
	@RequestMapping("/list")
	@ResponseBody
	@RequiresPermissions({"sys:menu:list"})
	public DataGridResult getPage(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		DataGridResult dgr = sysMenuService.getPageList(query);
		return dgr;
	}

    @RequestMapping("/info/{menuId}")
    @ResponseBody
    @RequiresPermissions({"sys:menu:info"})
    public R info(@PathVariable Long menuId) {
        SysMenu sysMenu = sysMenuService.getById(menuId);
        return R.ok().put("menu", sysMenu);
    }

    //查找顶级菜单
    @RequestMapping("/menu")
    @ResponseBody
    public R menu() {
        List<SysMenu> menuList = sysMenuService.getTopMenuList();
        return R.ok().put("menuList", menuList);
    }

    //角色授权菜单，所有菜单
    @RequestMapping("/all")
    @ResponseBody
    @RequiresPermissions({"sys:menu:all"})
    public R all() {
        //查询列表数据
        List<SysMenu> menuList = sysMenuService.findAll();
        return R.ok().put("menuList", menuList);
    }

	/*@RequestMapping("/select")
	@ResponseBody
	@RequiresPermissions({"sys:menu:select"})
	public R getPage() {
		List<SysMenu> list = sysMenuService.getNotButtonList();
		return R.ok().put("menuList", list);
	}
	*/
}
