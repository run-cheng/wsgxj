<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+
            request.getServerPort()+request.getContextPath()+"/";
%>--%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<title>汶上工信局系统</title>
<link rel="shortcut icon" href="/img/myimg/logo.icon">

<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<link href="/css/mycss/animate.css" rel="stylesheet">
<link href="/css/mycss/style.css" rel="stylesheet">
<body class="gray-bg">
	<div class="middle-box text-center loginscreen animated fadeInDown" id="dtApp">
		<div>
			<div>
				<h1 class="logo-name">R+</h1>
			</div>
			<h3>欢迎使用汶上工信局管理系统</h3>
			<div v-show="error.msg" class="alert alert-danger alert-dismissible">
				<h4 class="m-b-none"><i class="fa fa-exclamation-triangle">{{error.msg}}</i></h4>
			</div>
			<form class="m-t">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="用户名" v-model="user.username">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" placeholder="密码" v-model="user.password">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="验证码" v-model="user.kaptcha">
				</div>
				<div class="form-group">
					<img alt="验证码" v-bind:src="kaptcha" id="kaptcha" @click="refreshCode" style="cursor: pointer;">
					<a href="#" @click.prevent="refreshCode">点击刷新</a>
				</div>
				<div class="checkbox text-left">
					<label>
						<input type="checkbox" name="autoLogin" v-model="user.rememberMe" value="true">记住我
					</label>
				</div>
				<button type="button" class="btn btn-primary block full-width m-b" @click="login">登录</button>
				
				<!-- <p class="text-muted text-center">
					<a href="login.jsp#">
						<small>忘记密码了？</small>
					</a>
					|
					<a href="register.html">
						注册一个新账号
					</a>
				</p> -->
			</form>
			
		</div>
	</div>
	
	<!-- 全局js -->
	<script type="text/javascript" src="/plugins/jquery/jquery.js"></script>
	<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- vue -->
	<script type="text/javascript" src="/plugins/vue/vue.min.js"></script>
	<!-- 浮层 -->
	<script type="text/javascript" src="/plugins/layer/layer.js"></script>
	
	<script type="text/javascript">
		var vm = new Vue({
			el : '#dtApp',
			data : {
				user : {},
				error : {
					msg : ''
				},
				kaptcha : 'kaptcha.jpg'
			},
			methods : {
				login:function(){
					$.ajax({
						url : "sys/login",
						data : JSON.stringify(vm.user),
						type : "post",
						dataType : "json",
						contentType : "application/json",
						cache : false,
						success : function(r){
							var data = r;
							if(data.code == 0){
								window.location.href="index";
							}else{
								vm.error.msg = data.msg;
								vm.refreshCode();
							}
						}
					});
				},
				
				refreshCode : function(){
					$('#kaptcha').attr('src',vm.kaptcha);
				}
			}
		});
	</script>
</body>
</html>