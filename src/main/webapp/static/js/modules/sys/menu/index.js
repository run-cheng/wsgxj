
//菜单管理-列表
var vm = new Vue({
	el : '#dtApp',
	data : {

	},
	methods : {
		loadTable : function(){
			var columns = [
				{field:'name', title:'菜单名称'},
				{field:'parentMenu.name', title:'上级菜单'},
				{field:'icon', title:'菜单图标', formatter:function(value, row, index){
					return '<i class="'+value+' fa-lg"></li>';
				}},
				{field:'url', title:'菜单url'},
				{field:'perms', title:'授权标识'},
				{field:'type', title:'类型', formatter:function(value){
					switch(value){
					case 0:
						return '<span class="label label-primary">目录</span>';
						break;
					case 1:
						return '<span class="label label-success">菜单</span>';
						break;
					case 2:
						return '<span class="label label-warning">按钮</span>';
						break;
					}
				}},
				{field:'orderNum', title:'排序号'}
			];
			
			var option = T.btTableOption;
			var allColumns = option.columns.concat(columns);
			option.columns = allColumns;
			option.url = 'list';
			option.onPreBody = function(data){
				if(data.length > 0){
					var o = {rows : data};
					FastJson.format(o);
					return o.rows;
				}
			};
            option.queryParams = function(params){
                var menuName2 = $("#menuName2").val();
                var menuName = $("#menuName").val();
                if(menuName == menuName2){
                    return {
                        limit : params.limit,
                        offset : params.offset,
                        menuName : $("#menuName").val()
                    }
                }else{
                    return {
                        limit : params.limit,
                        offset : 0,
                        menuName : $("#menuName").val()
                    }
                }
            };
			option.responseHandler = function(res){
				return res;
			};
			option.onLoadSuccess = function(data){
                $("#menuName2").val($("#menuName").val());
				//console.log(data);
			};
			$('#table').bootstrapTable(option);
		},
		deleteBatch : function(){
			T.deleteMoreItem('menuId');
		},
        select : function(){
            $('#table').bootstrapTable('refresh',{
                url: 'list',
                method : 'post',
                contentType:"application/x-www-form-urlencoded",
                //striped : true,
                dataType : "json",
                pagination : true,
                sidePagination:'server',
                queryParamsType : "limit"
            });
        }
	}
});

$(function(){
	vm.loadTable();
})
