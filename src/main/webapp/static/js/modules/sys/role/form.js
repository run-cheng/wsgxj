

var vm = new Vue({
	el : '#dtApp',
	data : {
		title : null,
		ztree : null,
        type : null,
		role : {
			
		},
		setting : {
			data : {
				simpleData : {
					enable : true,//启用简单json
					idKey : "menuId",//主键oid
					pIdKey : "parentId",//关联的父记录的ID
					rootPId : null//修正根节点的父节点的数据
				},
				key : {
					url : 'nourl'//数据库中有url属性，但是不想跳转 
				}
			},
			check : {
				enable : true,
				nocheckInherit : true
			}
		}
		
	},
	methods : {
		init : function(){
			
			vm.getMenu();
			
			if(T.hasP('add')){
				vm.add();
			}
			if(T.hasP('edit')){
				vm.edit();
			}
            if(T.hasP('view')){
                vm.view();
            }
		},
		add : function(){
			vm.title = '新增';
			vm.type = 'add';
		},
		edit : function(){
			vm.title = '修改';
			vm.type = 'edit';

			//加载数据
			var roleId = T.p('id');
			if(!roleId){
				return;
			}
			$.get('info/'+ roleId,function(r){
				var data = eval(r);
				vm.role = data.role;
			})
		},
        view : function(){
            vm.title = '查看';
            vm.type = 'view';
			$("input").attr("disabled",true);
            //加载数据
            var roleId = T.p('id');
            if(!roleId){
                return;
            }
            $.get('info/'+ roleId,function(r){
                var data = eval(r);
                vm.role = data.role;
            })
        },
		saveOrUpdate : function(){
			
			//获取选择的菜单
			var nodes = vm.ztree.getCheckedNodes(true);
			var menuIdList = new Array();
			for(var i=0; i<nodes.length; i++){
				menuIdList.push(nodes[i].menuId);
			}
			vm.role.menuIdList = menuIdList;
			
			var url = vm.role.roleId == null ? 'add' : 'update';
			
			$.ajax({
				type : 'post',
				url : url,
				data : JSON.stringify(vm.role),
				contentType: "application/json;charset=UTF-8",
				success : function(r){
					var data = eval(r);
					if(data.code == 0){
						layer.alert('操作成功', function(index){
							window.location.href = 'index';
						});
					}else{
						layer.alert(data.msg);
					}
				}
			});
		},
		
		menuTree : function(){
			//自定页
			layer.open({
				  type: 1,
				  skin: 'layui-layer-demo', //样式类名
				  closeBtn: 0, //不显示关闭按钮
				  area: ['300px', '450px'],
				  anim: 2,
				  shadeClose: false, //开启遮罩关闭
				  content: $('#menuLayer'),
				  btn: ['确定', '取消'],
				  btn1: function(index){
					  //console.log('确定');
					  //获取ztree中被选中的节点
					  var nodes = vm.ztree.getSelectedNodes();
					  vm.menu.parentMenu = {
							  menuId : nodes[0].menuId,
							  name : nodes[0].name
					  };
					  
					  layer.close(index);
				  }
			});
		},
		
		getMenu: function(){
            var roleId = T.p('id');
		    var ids = "";
            if(roleId != null && roleId != undefined && roleId != ""){
                $.ajax({
                    type : 'post',
                    url : 'findMenuIdListByRoleId/'+roleId,
                    async : false,
                    contentType: "application/json;charset=UTF-8",
                    success : function(r){
                        ids = r.menuIdList;
                    }
                });
            }
			//加载菜单树
			$.get('../../sys/menu/all', function(r){
				var data = eval(r);
				//console.log(data);
				//初始化ztree
				vm.ztree = $.fn.zTree.init($("#menuTree"), vm.setting, data.menuList);
				//展开所有节点
				vm.ztree.expandAll(true);
				//console.log(ids);
				for(var i=0; i<ids.length; i++){
				    var node = vm.ztree.getNodeByParam("menuId",ids[i],null);
				    //console.log(node);
                    vm.ztree.checkNode( node, true, false);
                }
			});
		}
		
	}
});

$(function(){
	vm.init();
})