
//菜单管理-列表
var vm = new Vue({
	el : '#dtApp',
	data : {
		
	},
	methods : {
		loadTable : function(){
			var columns = [
				/*{field:'roleId', title:'角色ID'},*/
				{field:'roleName', title:'角色名称'},
				{field:'remark', title:'备注'},
				{field:'createTime', title:'创建时间',formatter:function(value,row,index){
                       return T.date(value);
                }},
				{field:'roleId', title:'操作', formatter:function(value,row,index){
					var button = '<a href="form?edit&id='+value+'"class="btn btn-info btn-xs btn-success">编辑</a>' +
                        '&nbsp;&nbsp;' +
                        '<a href="form?view&id='+value+'"class="btn btn-info btn-xs btn-success">查看</a>';

                        // if (index == 0) {
                        //     button =  '&emsp;&emsp;&emsp;' + '<a href="form?view&id='+value+'"class="btn btn-info btn-xs btn-success">查看</a>';
                        // }
					return button;
				}}
			];
			
			var option = T.btTableOption;
			var allColumns = option.columns.concat(columns);
			option.columns = allColumns;
			option.url = 'list';
			option.onPreBody = function(data){
				if(data.length > 0){
					var o = {rows : data};
					FastJson.format(o);
					return o.rows;
				}
			};
            option.queryParams = function(params){
                var roleName2 = $("#roleName2").val();
                var roleName = $("#roleName").val();
                if(roleName == roleName2){
                    return {
                        limit : params.limit,
                        offset : params.offset,
                        roleName : $("#roleName").val()
                    }
                }else{
                    return {
                        limit : params.limit,
                        offset : 0,
                        roleName : $("#roleName").val()
                    }
                }
            };
            option.responseHandler = function(res){
                return res;
            };
            option.onLoadSuccess = function(data){
                $("#roleName2").val($("#roleName").val());
                //console.log(data);
            };
			$('#table').bootstrapTable(option);
		},
		deleteBatch : function(){
			T.deleteMoreItem('roleId');
		},
        select : function(){
            $('#table').bootstrapTable('refresh',{
                url: 'list',
                method : 'post',
                contentType:"application/x-www-form-urlencoded",
                //striped : true,
                dataType : "json",
                pagination : true,
                sidePagination:'server',
                queryParamsType : "limit"
            });
        }
	}
});

$(function(){
	vm.loadTable();
})
