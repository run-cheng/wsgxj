
//菜单管理-列表
var vm = new Vue({
	el : '#dtApp',
	data : {
		
	},
	methods : {
		loadTable : function(){
			var columns = [
				/*{field:'id', title:'ID'},*/
				{field:'key', title:'参数名'},
				{field:'value', title:'参数值'},
				{field:'remark', title:'备注'},
				// {field:'id', title:'操作', formatter:function(value,row,index){
				// 	var button = '<a href="form?edit&id='+value+'"class="btn btn-info btn-xs btn-success">编辑</a>' +
                //         '&nbsp;&nbsp;' +
                //         '<a href="form?view&id='+value+'"class="btn btn-info btn-xs btn-success">查看</a>';
				// 	return button;
				// }}
			];
			
			var option = T.btTableOption;
			var allColumns = option.columns.concat(columns);
			option.columns = allColumns;
			option.url = 'list';
			/*option.onPreBody = function(data){
				if(data.length > 0){
					var o = {rows : data};
					//FastJson.format(o);
					return o.rows;
				}
			};*/
			$('#table').bootstrapTable(option);
		},
		deleteBatch : function(){
			T.deleteMoreItem('id');
		}
	}
});

$(function(){
	vm.loadTable();
})
