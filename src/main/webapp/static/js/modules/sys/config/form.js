

var vm = new Vue({
	el : '#dtApp',
	data : {
		title : null,
		ztree : null,
		config : {
		}
	},
	methods : {
		init : function(){
			if(T.hasP('add')){
				vm.add();
			}
			if(T.hasP('edit')){
				vm.edit();
			}
			if(T.hasP('info')){
			    vm.view();
            }
		},
		add : function(){
			vm.title = '新增';
		},
		edit : function(){
			vm.title = '修改';

			//加载数据
			var configId = T.p('id');
			if(!configId){
				return;
			}
			$.get('info/'+ configId,function(r){
				var data = r;
				vm.config = data.config;
			})
		},
        view :  function(){
            vm.title = '查看';
			$("input").attr("disabled",true);
            alert();
            //加载数据
            /*var configId = T.p('id');
            if(!configId){
                return;
            }
            $.get('info/'+ configId,function(r){
                console.log(r);
                var data = r;
                vm.config = data.config;
            })*/
        },
		saveOrUpdate : function(){
			
			var url = vm.config.id == null ? 'save' : 'update';
			
			$.ajax({
				type : 'post',
				url : url,
				data : JSON.stringify(vm.config),
				contentType: "application/json;charset=UTF-8",
				success : function(r){
					var data = r;
					if(data.code == 0){
						layer.alert('操作成功', function(index){
							window.location.href = 'index';
						});
					}else{
						layer.alert(data.msg);
					}
				}
			});
			
		}
	}
});

$(function(){
	vm.init();
})