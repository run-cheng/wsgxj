
//菜单管理-列表
var vm = new Vue({
	el : '#dtApp',
	data : {
		
	},
	methods : {
		loadTable : function(){
			var columns = [
				{field:'id', title:'id'},
				{field:'username', title:'用户名'},
				{field:'operation', title:'用户操作'},
				{field:'method', title:'请求方法'},
				{field:'params', title:'请求参数'},
				{field:'orderNum', title:'排序号'},
				{field:'ip', title:'ip地址'},
				{field:'createDate', title:'创建时间'}
			];
			
			var option = T.btTableOption;
			var allColumns = option.columns.concat(columns);
			option.columns = allColumns;
			option.url = 'list';
			option.onPreBody = function(data){
				if(data.length > 0){
					var o = {rows : data};
					FastJson.format(o);
					return o.rows;
				}
			};
			option.responseHandler = function(res){
				return res;
			};
			option.onLoadSuccess = function(data){
				console.log(data);
			};
			$('#table').bootstrapTable(option);
		},
		deleteBatch : function(){
			T.deleteMoreItem('menuId');
		}
	}
});

$(function(){
	vm.loadTable();
})
