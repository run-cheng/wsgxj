
//菜单管理-列表
var vm = new Vue({
	el : '#dtApp',
	data : {

	},
	methods : {
		loadTable : function(){
			var columns = [
				{field:'username', title:'用户名称'},
				{field:'password', title:'密码'},
				{field:'email', title:'邮箱'},
				{field:'mobile', title:'手机号'},
				{field:'status', title:'状态', formatter:function(value,row,index){
					switch(value){
					case 0:
						return '<span class="label label-warning">禁用</span>';
						break;
					case 1:
						return '<span class="label label-success">正常</span>';
						break;
					}
				}},
				{field:'createTime', title:'创建时间',formatter:function (value,row,index) {
					return T.date(value);
				}},
				{field:'userId', title:'操作', formatter:function(value,row,index){
					var shiroPermission = document.getElementById("shiroPermission").value;
					if(shiroPermission.indexOf("sys:user:update") >= 0){
						var button = '<a href="form?edit&id='+value+'"class="btn btn-info btn-xs btn-success">编辑</a>' +
                            '&nbsp;&nbsp;' +
                            '<a href="form?view&id='+value+'"class="btn btn-info btn-xs btn-success">查看</a>'
						return button;
					}
					return null;
				}}
			];
			
			var option = T.btTableOption;
			var allColumns = option.columns.concat(columns);
			option.columns = allColumns;
			option.url = 'list';
			option.onPreBody = function(data){
				if(data.length > 0){
					var o = {rows : data};
					FastJson.format(o);
					return o.rows;
				}
			};
            option.queryParams = function(params){
                var userName2 = $("#userName2").val();
                var userName = $("#userName").val();
                if(userName == userName2){
                    return {
                        limit : params.limit,
                        offset : params.offset,
                        userName : $("#userName").val()
                    }
                }else{
                    return {
                        limit : params.limit,
                        offset : 0,
                        userName : $("#userName").val()
                    }
                }
            };
            option.responseHandler = function(res){
                return res;
            };
            option.onLoadSuccess = function(data){
                $("#userName2").val($("#userName").val());
                //console.log(data);
            };
			$('#table').bootstrapTable(option);
		},
		deleteBatch : function(){
			T.deleteMoreItem('userId');
		},
        select : function(){
            $('#table').bootstrapTable('refresh',{
                url: 'list',
                method : 'post',
                contentType:"application/x-www-form-urlencoded",
                //striped : true,
                dataType : "json",
                pagination : true,
                sidePagination:'server',
                queryParamsType : "limit"
            });
        }
	}
});