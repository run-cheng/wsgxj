

var vm = new Vue({
	el : '#dtApp',
	data : {
		title : null,
		ztree : null,
        type : null,
		user : {
			status : 1,
			roleIdList : []
		}
	},
	methods : {
		init : function(){
			
			if(T.hasP('add')){
				vm.add();
			}
			if(T.hasP('edit')){
				vm.edit();
			}
			if(T.hasP('view')){
			    vm.view();
            }
		},
		add : function(){
			vm.title = '新增';
			vm.type = 'add';
		},
		edit : function(){
			vm.title = '修改';
			vm.type = 'edit';
			
			//加载数据
			var userId = T.p('id');
			if(!userId){
				return;
			}
			$.get('info/'+ userId,function(r){
				var data = r;
				vm.user = data.user;
			})
		},
        view : function(){
            vm.title = '查看';
            vm.type = 'view';
			$("input").attr("disabled",true);
            //加载数据
            var userId = T.p('id');
            if(!userId){
                return;
            }
            $.get('info/'+ userId,function(r){
                var data = r;
                vm.user = data.user;
            })
        },
		saveOrUpdate : function(){
			
			var url = vm.user.userId == null ? 'add' : 'update';
			var aaa = JSON.stringify(vm.user);
			console.log(aaa);
			
			$.ajax({
				type : 'post',
				url : url,
				data : JSON.stringify(vm.user),
				contentType: "application/json;charset=UTF-8",
				success : function(r){
					var data = r;
					if(data.code == 0){
						layer.alert('操作成功', function(index){
							window.location.href = 'index';
						});
					}else{
						layer.alert(data.msg);
					}
				}
			});
			
		}
	}
});

$(function(){
	vm.init();
})