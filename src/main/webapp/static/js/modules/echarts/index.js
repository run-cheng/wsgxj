document.write("<script language='javascript' src='/plugins/echarts/axios-0.18.0.js'></script>");
document.write("<script language='javascript' src='/plugins/vue/vue.min.js'></script>");


//三大门类规上工业企业分布
function enterprisesLocationByCategoryAndTime(){
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('enterprisesLocationByCategoryAndTime'));
    axios.get("/echartsData/getThreeMajorCategories").then((res) => {
            myChart.setOption(
                {
                    title: {
                        text: '三大门类规上工业企业分布',
                        subtext: '',
                        left: 'center'
                    },
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                    },
                    series: [
                        {
                            name: '访问来源',
                            type: 'pie',
                            radius: '50%',
                            data:res.data,
                            emphasis: {
                                itemStyle: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                }
            )
        }
    )

}


//重点行业规上工业企业个数
function keyIndustries() {
// 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('keyIndustries'));
    axios.get("/echartsData/getKeyIndustries").then((res) => {
            myChart.setOption(
                {
                    title: {
                        text: '重点行业规上工业企业个数',
                        subtext: '',
                        left: 'center'
                    },
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                    },
                    series: [
                        {
                            name: '访问来源',
                            type: 'pie',
                            radius: '50%',
                            data:res.data,
                            emphasis: {
                                itemStyle: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                }
            )
        }
    )
}

//规上工业企业个数
function enterpriseNumber(date) {
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('enterpriseNumber'));
    axios.get("/echartsData/getEnterpriseNumber?year=" + date).then((res) => {
            myChart.setOption(
                {
                    title: {
                        text: date + '规上工业企业个数',
                        subtext: ''
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['今年新增', '企业总数']
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            dataView: {show: true, readOnly: false},
                            magicType: {show: true, type: ['line', 'bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            data: res.data.name
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: '今年新增',
                            type: 'bar',
                            data: res.data.yearNum

                        },
                        {
                            name: '企业总数',
                            type: 'bar',
                            data: res.data.allNum

                        }
                    ]
                }
            )
        }
    )
}


//各镇街利润增幅%
function profitGrowth() {
    dateInit();
    var start = document.getElementById('start').value;
    var end = document.getElementById('end').value;
// 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('profitGrowth'));
    axios.get("/echartsData/getProfitGrowth?start=" + start + "&end=" + end).then((res) => {
    myChart.setOption(
        {
            title: {
                text: '各镇街利润增幅%',
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['增幅%']
            },
            toolbox: {
                show: true,
                feature: {
                    dataView: {show: true, readOnly: false},
                    magicType: {show: true, type: ['bar']},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    data: res.data.name
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: '增幅%',
                    type: 'bar',
                    data: res.data.growth
                }
            ]
        }
    )
    })
}

//汶上县规上工业企业营收累计增幅
function revenueCumulativeGrowth(){
    // 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('revenueCumulativeGrowth'));
    axios.get("/echartsData/getRevenueCumulativeGrowth").then((res) => {
            myChart.setOption(
                {
                    title: {
                        text: '汶上县规上工业企业营收累计增幅%',
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            dataView: {show: true, readOnly: false},
                            magicType: {show: true, type: ['bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    calculable: true,
                    xAxis: {
                        type: 'category',
                        data: res.data.xList
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        data: res.data.yList,
                        type: 'line'
                    }]
                }
            )
        }
    )
}

//汶上县规上工业企业利润累计增幅
function profitGrowthHas(){
// 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('profitGrowthHas'));
    axios.get("/echartsData/getProfitGrowthHas").then((res) => {
            myChart.setOption(
                {
                    title: {
                        text: '汶上县规上工业企业利润累计增幅%',
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            dataView: {show: true, readOnly: false},
                            magicType: {show: true, type: ['bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    calculable: true,
                    xAxis: {
                        type: 'category',
                        data: res.data.xList
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        data: res.data.yList,
                        type: 'line'
                    }]
                }
            )
        }
    )
}

//各镇街营业收入增幅
function revenueGrowth() {
    dateInit();
    var start = document.getElementById('start').value;
    var end = document.getElementById('end').value;
// 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('revenueGrowth'));
    axios.get("/echartsData/getRevenueGrowth?start=" + start + "&end=" + end).then((res) => {
            myChart.setOption(
                {
                    title: {
                        text: '各镇街营业收入增幅%'
                    },
                    tooltip: {
                        trigger: 'axis'
                    },
                    legend: {
                        data: ['增幅%']
                    },
                    toolbox: {
                        show: true,
                        feature: {
                            dataView: {show: true, readOnly: false},
                            magicType: {show: true, type: ['bar']},
                            restore: {show: true},
                            saveAsImage: {show: true}
                        }
                    },
                    calculable: true,
                    xAxis: [
                        {
                            type: 'category',
                            data: res.data.name
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: '增幅%',
                            type: 'bar',
                            data: res.data.growth
                        }
                    ]
                }
            )
        }
    )
}

//各镇街营收占全县比重饼状图
function revenueProportionTheCounty() {
    dateInit();
    var start = document.getElementById('start').value;
    var end = document.getElementById('end').value;
// 基于准备好的dom，初始化echarts实例
    var myChart = echarts.init(document.getElementById('revenueProportionTheCounty'));
    axios.get("/echartsData/getRevenueProportionTheCounty?start=" + start + "&end=" + end).then((res) => {
            myChart.setOption(
                {
                    title: {
                        text: '各镇街营收占全县比重饼状图',
                        left: 'center'
                    },
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        orient: 'vertical',
                        left: 'left',
                    },
                    series: [
                        {
                            name: '镇街名称',
                            type: 'pie',
                            radius: '50%',
                            data: res.data,
                            emphasis: {
                                itemStyle: {
                                    shadowBlur: 10,
                                    shadowOffsetX: 0,
                                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                                }
                            }
                        }
                    ]
                }
            )
        }
    )
}


function dateInit() {
    $('#start').datetimepicker({
        lang:'ch',
        format:"Y-m-d"     //格式化日期
    });
    $('#end').datetimepicker({
        lang:'ch',
        format:"Y-m-d"
    })
}

function reset() {
    document.getElementById('start').value = "";
    document.getElementById('end').value = "";
}