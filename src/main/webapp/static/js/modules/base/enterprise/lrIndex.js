
//菜单管理-列表
var vm = new Vue({
    el : '#dtApp',
    data : {

    },
    methods : {
        loadTable : function(){
            var columns = [
                {field:'thisMonthProfit', title:'本月利润'},
                {field:'lastYearSameMonthProfit', title:'去年同期利润'},
                {field:'monthIncrease', title:'月份增幅(%)'},
                {field:'thisYearGrandTotal', title:'本年累计'},
                {field:'lastYearGrandTotal', title:'去年累计'},
                {field:'yearIncrease', title:'年份增幅(%)'},
                {field:'time', title:'月份',formatter:function (value,row,index) {
                        return T.date(value);
                    }},
            ];

            var option = T.btTableOption;
            var allColumns = option.columns.concat(columns);
            option.columns = allColumns;
            option.url = 'lrList';
            option.onPreBody = function(data){
                if(data.length > 0){
                    var o = {rows : data};
                    FastJson.format(o);
                    return o.rows;
                }
            };
            option.queryParams = function(params){
                    return {
                        limit: params.limit,
                        offset: params.offset,
                        // val: $("#val").val(),
                        id: T.p("id")
                    }
            };
            option.responseHandler = function(res){
                return res;
            };
            // option.onLoadSuccess = function(data){
            //     $("#val2").val($("#val").val());
            //     //console.log(data);
            // };
            $('#table').bootstrapTable(option);
        },
        // deleteBatch : function(){
        //     T.deleteMoreItem('id');
        // },
        select : function(){
            $('#table').bootstrapTable('refresh',{
                url: 'lrList',
                method : 'post',
                contentType:"application/x-www-form-urlencoded",
                //striped : true,
                dataType : "json",
                pagination : true,
                sidePagination:'server',
                queryParamsType : "limit"
            });
        }
    }
});