

var vm = new Vue({
	el : '#dtApp',
	data : {
		title : null,
		ztree : null,
        type : null,
		enterprise : {
			deleteId : 0
		}
	},
	methods : {
		init : function(){
			
			if(T.hasP('add')){
				vm.add();
			}
			if(T.hasP('edit')){
				vm.edit();
			}
			if(T.hasP('view')){
			    vm.view();
            }
		},
		add : function(){
			vm.title = '新增';
			vm.type = 'add';
		},
		edit : function(){
			vm.title = '修改';
			vm.type = 'edit';
			
			//加载数据
			var enterpriseId = T.p('id');
			if(!enterpriseId){
				return;
			}
			$.get('info/'+ enterpriseId,function(r){
				var data = r;
				vm.enterprise = data.enterprise;
			})
		},
        view : function(){
            vm.title = '查看';
            vm.type = 'view';
            $("input").attr("disabled",true);
            //加载数据
            var enterpriseId = T.p('id');
            if(!enterpriseId){
                return;
            }
            $.get('info/'+ enterpriseId,function(r){
                var data = r;
                vm.enterprise = data.enterprise;
            })
        },
		saveOrUpdate : function(){
			
			var url = vm.enterprise.id == null ? 'add' : 'update';
			var aaa = JSON.stringify(vm.enterprise);
			console.log(aaa);
			
			$.ajax({
				type : 'post',
				url : url,
				data : JSON.stringify(vm.enterprise),
				contentType: "application/json;charset=UTF-8",
				success : function(r){
					var data = r;
					if(data.code == 0){
						layer.alert('操作成功', function(index){
							window.location.href = 'index';
						});
					}else{
						layer.alert(data.msg);
					}
				}
			});
			
		}
	}
});

$(function(){
	vm.init();
})