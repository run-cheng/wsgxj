
//菜单管理-列表
var vm = new Vue({
    el : '#dtApp',
    data : {

    },
    methods : {
        loadTable : function(){
            var columns = [
                {field:'name', title:'企业名称'},
                {field:'industryCode', title:'行业代码'},
                {field:'zjName',title:'所属镇街'},
                {field:'deleteId', title:'状态', formatter:function(value,row,index){
                        switch(value){
                            case 1:
                                return '<span class="label label-warning">禁用</span>';
                                break;
                            case 0:
                                return '<span class="label label-success">正常</span>';
                                break;
                        }
                    }},
                {field:'creationTime', title:'创建时间',formatter:function (value,row,index) {
                        return T.date(value);
                    }},
                {field:'id', title:'操作', formatter:function(value,row,index){
                        var shiroPermission = document.getElementById("shiroPermission").value;
                        if(shiroPermission.indexOf("base:enterprise:update") >= 0){
                            var button = '<a href="form?edit&id='+value+'"class="btn btn-info btn-xs btn-success">编辑</a>'/* +
                                '&nbsp;&nbsp;' +
                                '<a href="form?view&id='+value+'"class="btn btn-info btn-xs btn-success">查看</a>'*/ +
                                '&nbsp;&nbsp;' +
                                '<a href="ysIndex?full&id='+value+'"class="btn btn-info btn-xs btn-success">营收</a>' +
                                '&nbsp;&nbsp;' +
                                '<a href="lrIndex?full&id='+value+'"class="btn btn-info btn-xs btn-success">利润</a>';
                            return button;
                        }
                        return null;
                    }}
            ];

            var option = T.btTableOption;
            var allColumns = option.columns.concat(columns);
            option.columns = allColumns;
            option.url = 'list';
            option.onPreBody = function(data){
                if(data.length > 0){
                    var o = {rows : data};
                    FastJson.format(o);
                    return o.rows;
                }
            };
            option.queryParams = function(params){
                var val2 = $("#val2").val();
                var val = $("#val").val();
                if(val == val2){
                    return {
                        limit: params.limit,
                        offset: params.offset,
                        val: $("#val").val()
                    }
                } else {
                    return {
                        limit: params.limit,
                        offset: 0,
                        val: $("#val").val()
                    }
                }
            };
            option.responseHandler = function(res){
                return res;
            };
            option.onLoadSuccess = function(data){
                $("#val2").val($("#val").val());
                //console.log(data);
            };
            $('#table').bootstrapTable(option);
        },
        deleteBatch : function(){
            T.deleteMoreItem('id');
        },
        select : function(){
            $('#table').bootstrapTable('refresh',{
                url: 'list',
                method : 'post',
                contentType:"application/x-www-form-urlencoded",
                //striped : true,
                dataType : "json",
                pagination : true,
                sidePagination:'server',
                queryParamsType : "limit"
            });
        }
    }
});