

var vm = new Vue({
	el : '#dtApp',
	data : {
		title : null,
		ztree : null,
        type : null,
		industry : {
		}
	},
	methods : {
		init : function(){
			
			if(T.hasP('add')){
				vm.add();
			}
			if(T.hasP('edit')){
				vm.edit();
			}
			if(T.hasP('view')){
			    vm.view();
            }
		},
		add : function(){
			vm.title = '新增';
			vm.type = 'add';
		},
		edit : function(){
			vm.title = '修改';
			vm.type = 'edit';
			
			//加载数据
			var industryId = T.p('id');
			if(!industryId){
				return;
			}
			$.get('info/'+ industryId,function(r){
				var data = r;
				vm.industry = data.industry;
			})
		},
        view : function(){
            vm.title = '查看';
            vm.type = 'view';
            $("input").attr("disabled",true);
            //加载数据
            var industryId = T.p('id');
            if(!industryId){
                return;
            }
            $.get('info/'+ industryId,function(r){
                var data = r;
                vm.industry = data.industry;
            })
        },
		saveOrUpdate : function(){
			
			var url = vm.industry.id == null ? 'add' : 'update';
			var aaa = JSON.stringify(vm.industry);
			console.log(aaa);
			
			$.ajax({
				type : 'post',
				url : url,
				data : JSON.stringify(vm.industry),
				contentType: "application/json;charset=UTF-8",
				success : function(r){
					var data = r;
					if(data.code == 0){
						layer.alert('操作成功', function(index){
							window.location.href = 'index';
						});
					}else{
						layer.alert(data.msg);
					}
				}
			});
			
		}
	}
});

$(function(){
	vm.init();
})