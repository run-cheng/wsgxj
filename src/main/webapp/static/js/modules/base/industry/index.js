
//菜单管理-列表
var vm = new Vue({
    el : '#dtApp',
    data : {

    },
    methods : {
        loadTable : function(){
            var columns = [
                {field:'name', title:'行业名称'},
                {field:'industryCode',title:'行业编码'},
                {field:'id', title:'操作', formatter:function(value,row,index){
                        var shiroPermission = document.getElementById("shiroPermission").value;
                        if(shiroPermission.indexOf("base:industry:update") >= 0){
                            var button = '<a href="form?edit&id='+value+'"class="btn btn-info btn-xs btn-success">编辑</a>' +
                                '&nbsp;&nbsp;' +
                                /*'<a href="form?view&id='+value+'"class="btn btn-info btn-xs btn-success">查看</a>' +
                                '&nbsp;&nbsp;' +*/
                                '<a href="zIndex?view&id='+value+'"class="btn btn-info btn-xs btn-success">用电</a>'
                            return button;
                        }
                        return null;
                    }}
            ];

            var option = T.btTableOption;
            var allColumns = option.columns.concat(columns);
            option.columns = allColumns;
            option.url = 'list';
            option.onPreBody = function(data){
                if(data.length > 0){
                    var o = {rows : data};
                    FastJson.format(o);
                    return o.rows;
                }
            };
            option.queryParams = function(params){
                var name2 = $("#name2").val();
                var name = $("#name").val();
                if(name == name2){
                    return {
                        limit : params.limit,
                        offset : params.offset,
                        name : $("#name").val()
                    }
                }else{
                    return {
                        limit : params.limit,
                        offset : 0,
                        name : $("#name").val()
                    }
                }
            };
            option.responseHandler = function(res){
                return res;
            };
            option.onLoadSuccess = function(data){
                $("#name2").val($("#name").val());
                //console.log(data);
            };
            $('#table').bootstrapTable(option);
        },
        deleteBatch : function(){
            T.deleteMoreItem('userId');
        },
        select : function(){
            $('#table').bootstrapTable('refresh',{
                url: 'list',
                method : 'post',
                contentType:"application/x-www-form-urlencoded",
                //striped : true,
                dataType : "json",
                pagination : true,
                sidePagination:'server',
                queryParamsType : "limit"
            });
        }
    }
});