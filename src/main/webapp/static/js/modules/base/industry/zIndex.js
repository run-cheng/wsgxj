
//菜单管理-列表
var vm = new Vue({
    el : '#dtApp',
    data : {

    },
    methods : {
        loadTable : function(){
            var columns = [
                {field:'numberUsers', title:'用户个数'},
                {field:'assemblingCapacity', title:'装接容量'},
                {field:'thisMonthElectricity', title:'本月用电量'},
                {field:'lastYearSameMonthElectricity', title:'用电量上年同月'},
                {field:'monthIncrease', title:'月份增幅'},
                {field:'thisYearGrandTotal', title:'本年用电量累计'},
                {field:'lastYearGrandTotal', title:'去年用电量累计'},
                {field:'yearChange', title:'年份增减值'},
                {field:'yearIncrease', title:'累计同比'},
                {field:'proportionTheCounty', title:'全县比重'},
                {field:'monthIncreaseAnalysis', title:'波动分析'},
                {field:'unit', title:'单位'},
                {field:'caliber', title:'口径'},
                {field:'reviewStatus', title:'审核状态'},
                {field:'time', title:'月份',formatter:function (value,row,index) {
                        return T.date(value);
                    }},
            ];

            var option = T.btTableOption;
            var allColumns = option.columns.concat(columns);
            option.columns = allColumns;
            option.url = 'full';
            option.onPreBody = function(data){
                if(data.length > 0){
                    var o = {rows : data};
                    FastJson.format(o);
                    return o.rows;
                }
            };
            option.queryParams = function(params){
                    return {
                        limit: params.limit,
                        offset: params.offset,
                        id: T.p("id")
                    }
            };
            option.responseHandler = function(res){
                return res;
            };
            // option.onLoadSuccess = function(data){
            //     $("#val2").val($("#val").val());
            //     //console.log(data);
            // };
            $('#table').bootstrapTable(option);
        },
        // deleteBatch : function(){
        //     T.deleteMoreItem('id');
        // },
        select : function(){
            $('#table').bootstrapTable('refresh',{
                url: 'full',
                method : 'post',
                contentType:"application/x-www-form-urlencoded",
                //striped : true,
                dataType : "json",
                pagination : true,
                sidePagination:'server',
                queryParamsType : "limit"
            });
        }
    }
});