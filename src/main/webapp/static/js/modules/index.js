
Vue.component("menuItem",{
	props : ['item'],
	template : [
		'<li>',
			'<a href="#" class="J_menuItem_top">',
				'<i :class="item.icon"></i>',
				'<span class="nav-label">{{item.name}}</span>',
				'<span style="float:right;">🡒</span>',
                '<input type="hidden" value="true">',
			'</a>',
			'<ul class="nav nav-second-level" style="display:none;">',
				'<li v-if="child.type==1" v-for="child in item.childrenMenu">',
					'<a class="J_menuItem" :href="child.url">{{child.name}}</a>',
				'</li>',
			'</ul>',
		'</li>',
	].join('')
});

var vm = new Vue({
	
	el : '#wrapper',
	data : {
		menuList : {},
		 sites : [
             {text : 'Runnoob'},
             {text : 'Google'},
             {text : 'Taobao'}
       ]
	},
	created : function(){
		this.getMenuList();
	},
	methods : {
		init : function(){
			vm.getMenuList();
		},
		getMenuList : function(){
			$.get("sys/menu/menu", function(r){
				r = FastJson.format(r);
				//console.log(r);
				vm.menuList = r.menuList;
				vm.loadMenu();
			});
		},
		loadMenu : function(){
			//等待vue组件渲染成功后执行
			this.$nextTick(function(){
				//MetsiMenu
				$('#side-menu').metisMenu();

                $('.J_menuItem').on('click', menuItem);

				//通过遍历给菜单项加上data-index属性
				$(".J_menuItem").each(function(index){
					if(!$(this).attr('data-index')){
						$(this).attr('data-index', index);
					}
				});

				//给顶级菜单加上点击方法
                $(".J_menuItem_top").each(function(index){
                    $(this).on('click', function(){
                        var flag = $(this).children("input").val();
                        //console.log(flag);
                        if("true" == flag){
                            console.log($(this).children("span").eq(1));
                            $(this).children("span").eq(1).text("🡓");
                            $(this).children("input").val("false");
                            $(this).parent().children("ul").show();
                        }else{
                            $(this).children("span").eq(1).text("🡒");
                            $(this).children("input").val("true");
                            $(this).parent().children("ul").hide();
                        }
                    })
                })
			});
		}
	}
});

