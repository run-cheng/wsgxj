
//菜单管理-列表
var vm = new Vue({
	el : '#dtApp',
	data : {
		
	},
	methods : {
		loadTable : function(){
			var columns = [
				{field:'logId', title:'日志ID'},
				{field:'jobId', title:'任务ID'},
				{field:'beanName', title:'bean名称'},
				{field:'methodName', title:'方法名称'},
				{field:'params', title:'参数'},
				{field:'status', title:'状态',formatter : function(value){
					switch(value){
					case 0:
						return '<span class="label label-primary">成功</span>';
						break;
					case 1:
						return '<span class="label label-success">失败</span>';
						break;
					}
				}},
				{field:'times', title:'耗时(单位：毫秒)'},
				{field:'createTime', title:'执行时间'}
			];
			
			var option = T.btTableOption;
			var allColumns = option.columns.concat(columns);
			option.columns = allColumns;
			option.url = 'list';
			/*option.onPreBody = function(data){
				if(data.length > 0){
					var o = {rows : data};
					//FastJson.format(o);
					return o.rows;
				}
			};*/
			$('#table').bootstrapTable(option);
		},
		deleteBatch : function(){
			T.deleteMoreItem('jobLogId');
		}
	}
});

$(function(){
	vm.loadTable();
})
