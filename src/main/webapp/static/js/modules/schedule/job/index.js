
//菜单管理-列表
var vm = new Vue({
	el : '#dtApp',
	data : {
		
	},
	methods : {
		loadTable : function(){
			var columns = [
				{field:'jobId', title:'任务ID'},
				{field:'beanName', title:'bean名称'},
				{field:'methodName', title:'方法名称'},
				{field:'params', title:'参数'},
				{field:'cronExpression', title:'cron表达式'},
				{field:'stauts', title:'状态', formatter:function(value){
					switch(value){
					case 0:
						return '<span class="label label-primary">正常</span>';
						break;
					case 1:
						return '<span class="label label-success">暂停</span>';
						break;
					}
				}},
				{field:'remark', title:'备注'},
				{field:'createTime', title:'创建时间'},
				{field:'jobId', title:'操作', formatter:function(value,row,index){
					var edit = '<a href="form?edit&id='+value+'"class="btn btn-info btn-xs btn-success">编辑</a>'
					return edit;
				}}
			];
			
			var option = T.btTableOption;
			var allColumns = option.columns.concat(columns);
			option.columns = allColumns;
			option.url = 'list';
			/*option.onPreBody = function(data){
				if(data.length > 0){
					var o = {rows : data};
					//FastJson.format(o);
					return o.rows;
				}
			};*/
			$('#table').bootstrapTable(option);
		},
		deleteBatch : function(){
			T.deleteMoreItem('jobId');
		},
		pauseBatch : function(){
			T.doTask('jobId', 'pause');
		},
		resumeBatch : function(){
			T.doTask('jobId', 'resume');
		},
		runBatch : function(){
			T.doTask('jobId', 'run');
		}
	}
});

$(function(){
	vm.loadTable();
})
