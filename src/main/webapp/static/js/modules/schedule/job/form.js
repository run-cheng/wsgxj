

var vm = new Vue({
	el : '#dtApp',
	data : {
		title : null,
		job : {
		}
	},
	methods : {
		init : function(){
			
			if(T.hasP('add')){
				vm.add();
			}
			if(T.hasP('edit')){
				vm.edit();
			}
		},
		add : function(){
			vm.title = '新增';
		},
		edit : function(){
			vm.title = '修改';
			
			//加载数据
			var jobId = T.p('id');
			if(!jobId){
				return;
			}
			$.get('info/'+ jobId,function(r){
				var data = eval(r);
				vm.job = data.job;
			})
		},
		saveOrUpdate : function(){
			
			var url = vm.job.jobId == null ? 'save' : 'update';
			
			$.ajax({
				type : 'post',
				url : url,
				data : JSON.stringify(vm.job),
				contentType: "application/json;charset=UTF-8",
				success : function(r){
					var data = r;
					if(data.code == 0){
						layer.alert('操作成功', function(index){
							window.location.href = 'index';
						});
					}else{
						layer.alert(data.msg);
					}
				}
			});
			
		}
	}
});

$(function(){
	vm.init();
})