<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>汶上工信局系统</title>
    <link rel="shortcut icon" href="/img/myimg/logo.icon">

    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/plugins/fileinput/css/fileinput.min.css" rel="stylesheet"
          type="text/css">
    <link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="/css/mycss/animate.css" rel="stylesheet">
    <link href="/css/mycss/style.css" rel="stylesheet">

</head>
<body class="fixed-sidebar full-height-layout gray-bg" style="overflow:hidden">
<div id="wrapper" v-cloak>
    <!-- 左侧导航开始 -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="nav-close">
            <i class="fa fa-times-circle"></i>
        </div>
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<span class="clear">
									<span class="block m-t-xs">
										<strong class="font-bold"><div id="userName"><shiro:principal
                                                property="username"/></div></strong>
									</span>
									<span class="text-muted text-xs block">
										<c:forEach items="${sessionScope.roles }" var="role">${role } </c:forEach>
										<b class="caret"></b>
									</span>
								</span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <c:forEach items="${sessionScope.roles }" var="role">
                                <li><a href="#">${role }</a></li>
                            </c:forEach>
                            <li class="divider"></li>
                            <li>
                                <a href="logout">安全退出</a>
                            </li>
                        </ul>
                    </div>
                </li>
                					<li>
                						<a class="active J_menuTab" href="javascript:location.reload();" data-index="0">
                							<i class="fa fa-home">
                								<span>
                									主页
                								</span>
                							</i>
                						</a>
                					</li>


                <menu-item v-for="item in menuList" v-bind:item="item">

                </menu-item>
            </ul>
        </div>
    </nav>
    <!-- 左侧导航结束 -->
    <!-- 右侧部分开始 -->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom:0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary"></a>
                </div>
            </nav>
        </div>
        <div class="row content-tabs">
            <button class="roll-nav roll-left J_tableft">
                <i class="fa fa-backward"></i>
            </button>
            <nav class="page-tabs J_menuTabs">
                <div class="page-tabs-content">
<%--                    <a href="javascript:location.reload();" class="active J_menuTab" data-id="home" >首页</a>--%>
<%--                   <a href="javascript:" class="active J_menuTab " data-id="home" onclick="home()">首页</a>--%>
                </div>
            </nav>

            				<button class="roll-nav roll-right J_tabRight">
            					<i class="fa fa-forward"></i>
            				</button>
            				<div class="btn-group roll-nav roll-right" style="z-index:9999">
            					<button class="dropdown J_tabClose" data-toggle="dropdown">
            						关闭操作
            						<span class="caret"></span>
            					</button>
            					<ul role="menu" class="dropdown-menu dropdown-menu-right">
            						<li class="J_tabCloseAll">
            							<a href="javascript:location.reload();">关闭全部选项卡</a>
<%--            							<a>关闭全部选项卡</a>--%>
            						</li>
            						<li class="divider"></li>
            						<li class="J_tabCloseOther">
            							<a>关闭其他选项卡</a>
            						</li>
            					</ul>
            				</div>

            <a href="logout" class="roll-nav roll-right J_tabExit">
                <i class="fa fa fa-sign-out"></i>退出
            </a>
        </div>

        <div class="row J_mainContent" id="content-main">
            <%--				<input id="uploadFile" name="excelFile" class="file-loading"--%>
            <%--					   type="file" multiple accept=".xls,.xlsx" data-min-file-count="1"--%>
            <%--					   data-show-preview="true"> <br>--%>
            <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="home" frameborder="0"></iframe>
        </div>
        <div class="footer">
            <div class="pull-right">&copy;2017-2099
                <a href="http://www.gp1701.com/" target="_blank"></a>
            </div>
        </div>
    </div>
    <!-- 右侧部分结束 -->

    <!-- 右侧边栏开始 -->
    <div id="right-sidebar">
        <div class="sidebar-container">
            <div class="tab content">
                <div id="tab-1" class="tab-pane active">
                    <div class="sidebar-title">
                        <h3>
                            <i class="fa fa-commenls-o"></i>主题设置
                        </h3>
                        <small>
                            <i class="fa fa tim"></i>你可以从这里选择和预览主题的布局和样式，这些设置会被。。。
                        </small>
                    </div>
                    <div class="skin-settings">
                        <div class="title">主题设置</div>
                        <div class="settings-item">
                            <span>收起左侧菜单</span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" id="collapsemenu" name="collapsemenu"
                                           class="onoffswitch-checkbox">
                                    <label class="onoffswitch-label" for="collapsemenu">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="settings-item">
                            <span>固定宽度</span>
                            <div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" id="boxedlayout" name="boxedlayout"
                                           class="onoffswitch-checkbox">
                                    <label class="onoffswitch-label" for="boxedlayout">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="title">
                            皮肤选择
                        </div>
                        <div class="setings-item default-skin nb">
								<span class="skin-name">
									<a href="#" class="s-skin-0">默认皮肤</a>
								</span>
                        </div>
                        <div class="setings-item blue-skin nb">
								<span class="skin-name">
									<a href="#" class="s-skin-">蓝色主题</a>
								</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 右侧边栏结束 -->
</div>

<!-- 全局js -->
<script type="text/javascript" src="/plugins/jquery/jquery.js"></script>
<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- 文件上传js -->
<script type="text/javascript" src="/plugins/fileinput/js/fileinput.min.js"></script>
<script type="text/javascript" src="/plugins/fileinput/js/zh.js"></script>
<!-- vue -->
<script type="text/javascript" src="/plugins/vue/vue.min.js"></script>
<!-- fastjson -->
<script type="text/javascript" src="/plugins/fastjson/FastJson.js"></script>
<!-- 导航 -->
<script type="text/javascript" src="/plugins/metisMenu/jquery.metisMenu.js"></script>
<!-- 滚动条 -->
<script type="text/javascript"
        src="/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- 浮层 -->
<script type="text/javascript" src="/plugins/layer/layer.js"></script>
<!-- 进度条 -->
<script type="text/javascript" src="/plugins/pace/pace.min.js"></script>
<!-- 自定义js -->
<script type="text/javascript" src="/js/myjs/hplus.js"></script>
<script type="text/javascript" src="/js/myjs/contabs.js"></script>
<script type="text/javascript" src="/js/modules/index.js"></script>

<script>
    function home() {
        $(".J_iframe").attr("src","home")
    }
</script>
</body>
</html>