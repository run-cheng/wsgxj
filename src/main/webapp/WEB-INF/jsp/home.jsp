<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>管理系统</title>
<link rel="shortcut icon" href="/img/myimg/logo.icon">

<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<link href="/css/mycss/animate.css" rel="stylesheet">
<link href="/css/mycss/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<a href="/toUpload">
							<button type="button" class="btn btn-link btn-lg">数据上传</button>
						</a>
						<a href="https://www.baidu.com/" target="view_window">
							<button type="button" class="btn btn-link btn-lg">短信平台</button>
						</a>
					</div>
					<div class="ibox-content">
						<div class="well">欢迎登录汶上工信局系统</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 全局js -->
	<script type="text/javascript" src="/plugins/jquery/jquery.js"></script>
	<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>


	<!-- ECharts -->
	<%-- <script type="text/javascript" src="/libs /echarts/echarts-all.js"></script> --%>
	<script>

	</script>
</body>
</html>