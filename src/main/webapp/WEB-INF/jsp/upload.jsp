<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>文件上传</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous">
    <link href="/plugins/fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="/plugins/fileinput/js/fileinput.min.js" type="text/javascript"></script>
    <script src="/plugins/fileinput/js/zh.js" type="text/javascript"></script>
    <script src="/plugins/fileinput/js/fas_theme.min.js" type="text/javascript"></script>
    <style type="text/css">
        #app{
               width: 98%;
               margin:0 auto;
               box-shadow: 10px 10px 5px #888888;
               padding:5px 5px 5px 5px;
           }
        #but{
            width: 98%;
            margin:0 auto;
            padding:5px 5px 5px 5px;
        }
    </style>
</head>
<body>
    <div id="but">
        <a href="/getElectricityTemplate">
            <button type="button" class="btn btn-primary">用电量模板</button>
        </a>&nbsp;&nbsp;
        <a href="/getEnterpriseTemplate">
            <button type="button" class="btn btn-primary">企业营收模板</button>
        </a>
    </div>
    <div id="app">
        <input id="file" name="file" type="file">
    </div>
</body>
<script>
    $("#file").fileinput({
        theme: 'fas',
        language: 'zh',//设置语言
        uploadUrl: '/upload' ,//上传路径
        uploadAsync: false,//同步上传
        enctype: 'multipart/form-data',
        maxFileCount:1,//最大上传文件数
        allowedFileExtensions: ['xlsx', 'xls']
    });

    $("#file").on('fileuploaderror', function(event, data, msg) {
        console.log('File uploaded', event,data.previewId, data, data.fileId, msg);
    });

    $('＃file').fileinput('reset').trigger('custom-event');

</script>
</html>