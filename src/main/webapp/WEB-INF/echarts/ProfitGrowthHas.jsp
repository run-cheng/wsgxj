<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>汶上工信局系统</title>
<link rel="shortcut icon" href="/img/myimg/logo.icon">

<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- bootstrap table -->
<link rel="stylesheet" href="/plugins/bootstrap-table/bootstrap-table.min.css">

<link href="/css/mycss/animate.css" rel="stylesheet">
<link href="/css/mycss/style.css" rel="stylesheet">

<!-- 自定义css -->
<link href="/css/mycss/my.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<script src="/plugins/jquery/jquery.js"></script>
</head>
<body class="my-content">
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    <div id="profitGrowthHas" style="width: 1000px;height:600px; margin: 0 auto;"></div>

	<!-- 全局js -->
	<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/plugins/vue/vue.min.js"></script>

	<!-- 浮层 -->
	<script type="text/javascript" src="/plugins/layer/layer.js"></script>

	<!-- bootstrap table -->
	<script type="text/javascript" src="/plugins/bootstrap-table/bootstrap-table.min.js"></script>
	<script type="text/javascript" src="/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

	<!--echarts-->
    <script type="text/javascript" src="/plugins/echarts/echarts.js"></script>

    <!-- 全局通用js -->
	<script type="text/javascript" src="/js/myjs/common.js"></script>

	<!-- 模块 -->
<%--	<script type="text/javascript" src="/echarts/index.js"></script>--%>
	<script type="text/javascript" src="/js/modules/echarts/index.js"></script>


	<script>
		$(//初始化echarts图表
            profitGrowthHas()
		)
	</script>

</body>
</html>