<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>汶上工信局系统</title>
<link rel="shortcut icon" href="/img/myimg/logo.icon">

<link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- bootstrap table -->
<link rel="stylesheet" href="/plugins/bootstrap-table/bootstrap-table.min.css">

<link href="/css/mycss/animate.css" rel="stylesheet">
<link href="/css/mycss/style.css" rel="stylesheet">

<!-- 自定义css -->
<link href="/css/mycss/my.css" rel="stylesheet">

</head>
<body class="my-content">
	<div id="dtApp" v-cloak>
        <div class="form-inline" id="toolbar">
            <shiro:hasPermission name="sys:menu:list">
                <div class="form-group">
                    &nbsp;
                    <label class="sr-only" for="menuName">菜单名称</label>
                    <input type="text" class="form-control" name="menuName" id="menuName" placeholder="请输入菜单名称">
                    <input type="hidden" value="" id="menuName2">
                </div>
                <a @click="select" class="btn btn-primary" type="button">
                    <i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">搜索</span>
                </a>
            </shiro:hasPermission>
        </div>
		<table id="table" class="table table-bordered"></table>
	</div>
	<!-- 全局js -->
	<script type="text/javascript" src="/plugins/jquery/jquery.js"></script>
	<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/plugins/vue/vue.min.js"></script>
	
	<!-- fastjson -->
	<script type="text/javascript" src="/plugins/fastjson/FastJson.js"></script>
	
	<!-- 浮层 -->
	<script type="text/javascript" src="/plugins/layer/layer.js"></script>
	
	<!-- bootstrap table -->
	<script type="text/javascript" src="/plugins/bootstrap-table/bootstrap-table.min.js"></script>
	<script type="text/javascript" src="/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
	
	<!-- 全局通用js -->
	<script type="text/javascript" src="/js/myjs/common.js"></script>
	
	<!-- 模块 -->
	<script type="text/javascript" src="/js/modules/sys/menu/index.js"></script>
</body>
</html>