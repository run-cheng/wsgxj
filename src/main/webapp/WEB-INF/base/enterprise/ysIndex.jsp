<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>汶上工信局系统</title>
    <link rel="shortcut icon" href="/img/myimg/logo.icon">

    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- bootstrap table -->
    <link rel="stylesheet" href="/plugins/bootstrap-table/bootstrap-table.min.css">

    <link href="/css/mycss/animate.css" rel="stylesheet">
    <link href="/css/mycss/style.css" rel="stylesheet">

    <!-- 自定义css -->
    <link href="/css/mycss/my.css" rel="stylesheet">

</head>
<body class="my-content">
<%--<input type="hidden" value="${sessionScope.shiroPermission }" id="shiroPermission">--%>
<input type="hidden" id="shiroPermission">
<div id="dtApp" v-cloak>
    <div class="form-inline" id="toolbar">
        <a href="index">
        <button class="btn btn-primary" type="button">
            &nbsp;&nbsp;<span class="bold">返回</span>
        </button>
        </a>
    </div>
    <table id="table" class="table table-bordered"></table>

</div>
<!-- 全局js -->
<script type="text/javascript" src="/plugins/jquery/jquery.js"></script>
<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/plugins/vue/vue.min.js"></script>

<!-- fastjson -->
<script type="text/javascript" src="/plugins/fastjson/FastJson.js"></script>

<!-- 浮层 -->
<script type="text/javascript" src="/plugins/layer/layer.js"></script>

<!-- bootstrap table -->
<script type="text/javascript" src="/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script type="text/javascript" src="/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

<!-- 全局通用js -->
<script type="text/javascript" src="/js/myjs/common.js"></script>

<!-- 模块 -->
<script type="text/javascript" src="/static/js/modules/base/enterprise/ysIndex.js"></script>

<script>
    $(function(){
        vm.loadTable();
    })
</script>
</body>
</html>