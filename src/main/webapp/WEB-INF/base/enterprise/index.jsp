<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>汶上工信局系统</title>
    <link rel="shortcut icon" href="/img/myimg/logo.icon">

    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- bootstrap table -->
    <link rel="stylesheet" href="/plugins/bootstrap-table/bootstrap-table.min.css">

    <link href="/css/mycss/animate.css" rel="stylesheet">
    <link href="/css/mycss/style.css" rel="stylesheet">

    <!-- 自定义css -->
    <link href="/css/mycss/my.css" rel="stylesheet">

</head>
<body class="my-content">
<%--<input type="hidden" value="${sessionScope.shiroPermission }" id="shiroPermission">--%>
<input type="hidden" id="shiroPermission">
<div id="dtApp" v-cloak>
    <div class="form-inline" id="toolbar">
        <shiro:hasPermission name="base:enterprise:list">
            <div class="form-group">
                &nbsp;
                <label class="sr-only" for="val">企业</label>
                <input type="text" class="form-control" name="val" id="val" placeholder="企业名称/行业代码/镇街">
                <input type="hidden" value="" id="val2">
            </div>
            <a @click="select" class="btn btn-primary" type="button">
                <i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">搜索</span>
            </a>
        </shiro:hasPermission>
        <shiro:hasPermission name="base:enterprise:add">
            <a href="form?add" class="btn btn-success" type="button">
                <i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">新增</span>
            </a>
        </shiro:hasPermission>
        <shiro:hasPermission name="base:enterprise:delete">
            <button class="btn btn-danger" type="button" @click="deleteBatch">
                <i class="fa fa-remove"></i>&nbsp;&nbsp;<span class="bold">删除</span>
            </button>
        </shiro:hasPermission>
    </div>
    <table id="table" class="table table-bordered"></table>

    <%--<shiro:hasPermission name="user:updatePassword">
        <script type="text/javascript">
                function edit(){
                    $('.edit').css('display','');//display不用block原因是避免改变行间样式
                }
        </script>
    </shiro:hasPermission>--%>
</div>
<!-- 全局js -->
<script type="text/javascript" src="/plugins/jquery/jquery.js"></script>
<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/plugins/vue/vue.min.js"></script>

<!-- fastjson -->
<script type="text/javascript" src="/plugins/fastjson/FastJson.js"></script>

<!-- 浮层 -->
<script type="text/javascript" src="/plugins/layer/layer.js"></script>

<!-- bootstrap table -->
<script type="text/javascript" src="/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script type="text/javascript" src="/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>

<!-- 全局通用js -->
<script type="text/javascript" src="/js/myjs/common.js"></script>

<!-- 模块 -->
<script type="text/javascript" src="/js/modules/base/enterprise/index.js"></script>

<script>
    $(function(){
        var shiroPermission = "${shiroPermission}";
        $("#shiroPermission").val(shiroPermission);
        vm.loadTable();
    })
</script>
</body>
</html>