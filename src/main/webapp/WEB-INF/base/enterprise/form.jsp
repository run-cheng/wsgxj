<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>汶上工信局系统</title>
    <link rel="shortcut icon" href="/img/myimg/logo.icon">

    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- bootstrap table -->
    <link rel="stylesheet" href="/plugins/bootstrap-table/bootstrap-table.min.css">
    <!--   验证 -->
    <link rel="stylesheet" href="/plugins/bootstrap-validator/css/bootstrapValidator.min.css"/>

    <link href="/css/mycss/animate.css" rel="stylesheet">
    <link href="/css/mycss/style.css" rel="stylesheet">

    <!-- ztree -->
    <link href="/plugins/zTree_v3/css/metroStyle/metroStyle.css" rel="stylesheet">

    <!-- 自定义css -->
    <link href="/css/mycss/my.css" rel="stylesheet">

</head>
<body class="my-content">
<div id="dtApp" v-cloak>
    <div class="panel panel-default">
        <div class="panel-heading">{{title}}</div>
        <div class="panel-body">
            <form class="form-horizontal" id="form-test">
                <div class="form-group">
                    <div class="col-sm-2 control-label">企业名称</div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" v-model="enterprise.name" autocomplete="new-password" id="name" placeholder="企业名称">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2 control-label">行业代码</div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" v-model="enterprise.industryCode" autocomplete="new-password" id="code" placeholder="行业代码">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 control-label">所属镇街</div>
                    <div class="form-group" style="margin: 0 auto" id="zjId">
                        <c:forEach items="${zjList}" var="zj">
                            <label class="radio-inline">
                                <input type="radio" name="zjIdList" v-model="enterprise.zjTownBackStreetsId" value="${zj.id}">
                                    ${zj.name}
                            </label>
                        </c:forEach>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2 control-label">类型</div>
                    <div class="form-group">
                        <label class="radio-inline">
                            <input type="radio" name="status" v-model="enterprise.deleteId" value="1">
                            禁用
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="status" v-model="enterprise.deleteId" value="0">
                            正常
                        </label>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>
                <div class="form-group pull-right">
                    <div class="col-sm-12">
                        <shiro:hasPermission name="sys:user:save">
                            <button v-if="type == 'edit' || type == 'add'" class="btn btn-primary" type="button" @click="saveOrUpdate" id="btn-test">保存内容</button>
                        </shiro:hasPermission>
                        <a href="index" class="btn btn-white">取消</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- 全局js -->
<script type="text/javascript" src="/plugins/jquery/jquery.js"></script>
<script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/plugins/vue/vue.min.js"></script>
<script type="text/javascript" src="/plugins/bootstrap-validator/js/bootstrapValidator.min.js"></script>
<!-- 浮层 -->
<script type="text/javascript" src="/plugins/layer/layer.js"></script>

<!-- ztree -->
<script type="text/javascript" src="/plugins/zTree_v3/js/jquery.ztree.all.min.js"></script>

<!-- fastjson -->
<script type="text/javascript" src="/plugins/fastjson/FastJson.js"></script>

<!-- 全局通用js -->
<script type="text/javascript" src="/js/myjs/common.js"></script>

<!-- 模块 -->
<script type="text/javascript" src="/js/modules/base/enterprise/form.js"></script>

</body>
</html>